# ![](README_LOGO.png) Safety car 

Car Insurance web API - allows simulation of quote amount and request insurance policies online. The application functionalities are related to three different type of users: public, private and administrators. Client can create and print an offer or receive it on email. Agents can find requests by different criteria, approve/declined clients policies.

## Functionalities


```
sign-up - with confirmation email
login 
get quote  
create policy 
list history of user’s requests
cancel requst
approve/ decline request
create PDF
send confirmation email
send emal with attachment PDF
filter all requests by different criteria
visualize table from database
upload picture
upload excel table
```

#### [STEP 1 - Home screen](#)

•	allows non authenticated users to :
-	get quote by choosing between different cars > model, make, cubic capacity 
-	calculate total premium based on their car , age and previous accidents 
-	log in if they want to create policy request 

#### [STEP 2 - Register screen](#)

•	register user by sending confirmation mail with 24 hours activation period

#### [STEP 3 - Log in screen](#)

•	log in alredy enabled users

#### [STEP 4 - User screen](#)

•	user can see all his / her requst in cards or table view

•	user can see in details every policy of his / her list

•	user can cancel request if status of policy is Pending - request has been removed from user's list

•	user can generate PDF of policy in new window

•	user can send current policy PDF by email

#### [STEP 5 - Agent screen](#)

•	agent can filter requests by different criteria :
-	 by status
-	 by policy user phone
-	 by policy user email
-	 by request date - from date, to date or between two dates

•	agent can approve or decline user requests 

•	send email - when policy status has been changed by agent

•	open policy picture - to compare with policy info

•	see active coefficient tables 

#### [STEP 6 - Admin screen](#)

•	have all user and agent functionality

•	admin can upload excel table to database / all old tables became with status unactive


#### [ Contact screen](#)

•	google map to find us

#### [About screen](#)

•	just for fun


## Hosting

Address of hosting here 

## Resoursec

*  Spring Boot
*  Hibernate
*  JWT Security
*  MySQL
*  jQuery
*  Mockito
*  Swagger
*  Gradle
*  Bootstrap 4.3.1
*  Handlebars : 4.1.1
*  Trello

## Contributors

* [Viktoria Kovacheva](https://gitlab.com/voxa?nav_source=navbar)
* [Nikola Pazderov](https://gitlab.com/NPazderov)

# ![](swagger.png)
# ![](swagger2.png)
# ![](db.png)
