const onLoadHome = (handler) => handler()

const onClickHome = (handler) => {
  $(document).on('click', '#home', handler)
}

const onClickAbout = (handler) => {
  $(document).on('click', '#about', handler)
}

const onClickRegister = (handler) => {
  $(document).on('click', '#register', handler)
}

const onClickLogin = (handler) => {
  $(document).on('click', '#loginHeader', handler)
}

const onClickOffer = (handler) => {
  $(document).on('click', '#offer', handler)
}

const onClickLoginButton = (handler) => {
  $(document).on('click', '#login', handler)
}

const onClickGetStarted = (handler) => {
  $(document).on('click', '#getStarted', handler)
}

const onClickMakeOption = (handler) => {
  $(document).on('click', '#makeOption', handler)
}

const onClickModelOption = (handler) => {
  $(document).on('click', '#modelOption', handler)
}

const onClickDriverInfo = (handler) => {
  $(document).on('click', '#driverInfo', handler)
}

const onClickMyRequests = (handler) => {
  $(document).on('click', '#myRequests', handler)
}
const onClickRequestCardInfo = (handler) => {
  $(document).on('click', '.requestCardDetail', handler)
}

const onClickContact = (handler) => {
  $(document).on('click', '#contact', handler)
}

const onClickMyProfile = (handler) => {
  $(document).on('click', '#myProfile', handler)
}

const onClickRequestTable = (handler) => {
  $(document).on('click', '#requestTable', handler)
}

const onClickGetPrice = (handler) => {
  $(document).on('click', '#getPrice', handler)
}

const onClickOfferBack = (handler) => {
  $(document).on('click', '#offerBack', handler)
}

const onClickDriverBack = (handler) => {
  $(document).on('click', '#driverBack', handler)
}

const onClickODBack = (handler) => {
  $(document).on('click', '#ODBack', handler)
}

const onClickNo = (handler) => {
  $(document).on('click', '#no', handler)
}

const onClickYes = (handler) => {
  $(document).on('click', '#yes', handler)
}

const onClickLogout = (handler) => {
  $(document).on('click', '#logout', handler)
}

const onClickFilterPage = (handler) => {
  $(document).on('click', '#filterPage', handler)
}

const onClickAllRequests = (handler) => {
  $(document).on('click', '#allRequests', handler)
}

const onClickAllPendingRequests = (handler) => {
  $(document).on('click', '#allPendingRequests', handler)
}

const onClickAllApprovedRequests = (handler) => {
  $(document).on('click', '#allApprovedRequests', handler)
}

const onClickAllDeclinedRequests = (handler) => {
  $(document).on('click', '#allDeclinedRequests', handler)
}

const onClickClearAllFilters = (handler) => {
  $(document).on('click', '#clearFiltersButton', handler)
}

const onClickSearchByPhoneNumber = (handler) => {
  $(document).on('click', '#searchByPhoneNumber', handler)
}

const onClickSearchByEmail = (handler) => {
  $(document).on('click', '#searchByEmail', handler)
}

const onClickApproveRequest = (handler) => {
  $(document).on('click', '#approveRequest', handler)
}

const onClickDeclineRequest = (handler) => {
  $(document).on('click', '#declineRequest', handler)
}

const onClickAllUsers = (handler) => {
  $(document).on('click', '#allUsers', handler)
}

const onClickPolicyModalLogin = (handler) => {
  $(document).on('click', '#policyModalLogin', handler)
}


const onClickRequestPolicy = (handler) => {
  $(document).on('click', '#requestPolicy', handler)
}

const onClickRegDate = (handler) => {
  $(document).on('click', '#regDate', handler)
}

const onClickCancelRequest = (handler) => {
  $(document).on('click', '#cancelRequest', handler)
}

const onClickSavePolicy = (handler) => {
  $(document).on('click', '#savePolicy', handler)
}

const onClickEditInfo = (handler) => {
  $(document).on('click', '#editInfo', handler)
}

const onClickCreateAccount = (handler) => {
  $(document).on('click', '#createAccount', handler)
}

const onClickUploadTable = (handler) => {
  $(document).on('click', '#uploadTable', handler)
}

const onClickUpload = (handler) => {
  $(document).on('click', '#upload', handler)
}

const onClickFilterByRequestDate = (handler) => {
  $(document).on('click', '#filterByRequestDate', handler)
}

const onClickPrintPDF = (handler) => {
  $(document).on('click', '#printPDF', handler)
}

const onClickSendPDFEmail = (handler) => {
  $(document).on('click', '#policyPDFEmail', handler)
}

const onClickShow = (handler) => {
  $(document).on('click', '#show', handler)
}

const onClickShowInsCoeff = (handler) => {
  $(document).on('click', '#showInsCoeff', handler)
}

const onClickShowInsTaxes = (handler) => {
  $(document).on('click', '#showInsTaxes', handler)
}

const onClickNextPage = (handler) => {
  $(document).on('click', '#next', handler)
}

const onClickPreviousPage = (handler) => {
  $(document).on('click', '#previous', handler)
}

const onClickPage = (handler) => {
  $(document).on('click', '.page', handler)
}

export {
  onLoadHome,
  onClickHome,
  onClickAbout,
  onClickRegister,
  onClickLogin,
  onClickLoginButton,
  onClickOffer,
  onClickGetStarted,
  onClickMakeOption,
  onClickModelOption,
  onClickDriverInfo,
  onClickMyRequests,
  onClickRequestCardInfo,
  onClickContact,
  onClickMyProfile,
  onClickRequestTable,
  onClickGetPrice,
  onClickOfferBack,
  onClickDriverBack,
  onClickODBack,
  onClickNo,
  onClickYes,
  onClickLogout,
  onClickAllRequests,
  onClickAllPendingRequests,
  onClickAllApprovedRequests,
  onClickAllDeclinedRequests,
  onClickFilterPage,
  onClickClearAllFilters,
  onClickSearchByPhoneNumber,
  onClickSearchByEmail,
  onClickApproveRequest,
  onClickDeclineRequest,
  onClickAllUsers,
  onClickPolicyModalLogin,
  onClickRequestPolicy,
  onClickRegDate,
  onClickCancelRequest,
  onClickSavePolicy,
  onClickEditInfo,
  onClickCreateAccount,
  onClickUploadTable,
  onClickUpload,
  onClickFilterByRequestDate,
  onClickPrintPDF,
  onClickSendPDFEmail,
  onClickShow,
  onClickShowInsCoeff,
  onClickShowInsTaxes,
  onClickNextPage,
  onClickPreviousPage,
  onClickPage
}