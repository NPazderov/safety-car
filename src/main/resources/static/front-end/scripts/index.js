import {
  registerPartials
} from './partials.js'

import {
  onLoadHome,
  onClickHome,
  onClickAbout,
  onClickRegister,
  onClickLogin,
  onClickOffer,
  onClickLoginButton,
  onClickGetStarted,
  onClickMakeOption,
  onClickModelOption,
  onClickMyRequests,
  onClickRequestCardInfo,
  onClickContact,
  onClickMyProfile,
  onClickRequestTable,
  onClickGetPrice,
  onClickOfferBack,
  onClickDriverBack,
  onClickODBack,
  onClickNo,
  onClickYes,
  onClickLogout,
  onClickAllRequests,
  onClickAllPendingRequests,
  onClickAllApprovedRequests,
  onClickAllDeclinedRequests,
  onClickFilterPage,
  onClickClearAllFilters,
  onClickSearchByPhoneNumber,
  onClickSearchByEmail,
  onClickApproveRequest,
  onClickDeclineRequest,
  onClickAllUsers,
  onClickPolicyModalLogin,
  onClickRequestPolicy,
  onClickRegDate,
  onClickCancelRequest,
  onClickSavePolicy,
  onClickEditInfo,
  onClickCreateAccount,
  onClickDriverInfo,
  onClickUploadTable,
  onClickUpload,
  onClickFilterByRequestDate,
  onClickPrintPDF,
  onClickSendPDFEmail,
  onClickShow,
  onClickShowInsTaxes,
  onClickShowInsCoeff,
  onClickNextPage,
  onClickPreviousPage,
  onClickPage
} from './events.js'

import {
  handleOnLoadHome,
  handleClickHome,
  handleClickAbout,
  handleClickRegister,
  handleClickLogin,
  handleClickOffer,
  handleClickLoginButton,
  handleClickGetStarted,
  handleClickMakeOption,
  handleClickModelOption,
  handleClickMyRequests,
  handleClickRequestCardInfo,
  handleClickContact,
  handleClickMyProfile,
  handleClickRequestTable,
  handleClickGetPrice,
  handleClickOfferBack,
  handleClickODBack,
  handleClickNo,
  handleClickYes,
  handleClickLogout,
  handleClickAllRequests,
  handleClickAllPendingRequests,
  handleClickAllApprovedRequests,
  handleClickAllDeclinedRequests,
  handleClickFilterPage,
  handleClickClearAllFilters,
  handleClickSearchByPhoneNumber,
  handleClickSearchByEmail,
  handleClickApproveRequest,
  handleClickDeclineRequest,
  handleClickAllUsers,
  handleClickOnPolicyModalLogin,
  handleClickRequestPolicy,
  handleClickOnRegDate,
  handleClickCancelRequest,
  handleClickSavePolicy,
  handleClickEditUser,
  handleClickCreateAccount,
  handleClickDriverInfo,
  handleClickUploadTable,
  handleClickUpload,
  handleClickFilterByRequestDate,
  handleClickPrintPDF,
  handleClickSendPDFEmail,
  handleClickShow,
  handleClickShowInsTaxes,
  handleClickShowInsCoeff,
  handleClickNextPage,
  handleClickPreviousPage,
  handleClickPage
} from './handlers.js'

$(async () => {
  registerPartials()
  onLoadHome(handleOnLoadHome)
  onClickHome(handleClickHome)
  onClickAbout(handleClickAbout)
  onClickRegister(handleClickRegister)
  onClickLogin(handleClickLogin)
  onClickLoginButton(handleClickLoginButton)
  onClickOffer(handleClickOffer)
  onClickGetStarted(handleClickGetStarted)
  onClickMakeOption(handleClickMakeOption)
  onClickModelOption(handleClickModelOption)
  onClickDriverInfo(handleClickDriverInfo)
  onClickMyRequests(handleClickMyRequests)
  onClickRequestCardInfo(handleClickRequestCardInfo)
  onClickContact(handleClickContact)
  onClickMyProfile(handleClickMyProfile)
  onClickRequestTable(handleClickRequestTable)
  onClickGetPrice(handleClickGetPrice)
  onClickOfferBack(handleClickOfferBack)
  onClickDriverBack(handleClickOffer)
  onClickODBack(handleClickODBack)
  onClickNo(handleClickNo)
  onClickYes(handleClickYes)
  onClickLogout(handleClickLogout)
  onClickAllRequests(handleClickAllRequests)
  onClickAllPendingRequests(handleClickAllPendingRequests)
  onClickAllApprovedRequests(handleClickAllApprovedRequests)
  onClickAllDeclinedRequests(handleClickAllDeclinedRequests)
  onClickFilterPage(handleClickFilterPage)
  onClickClearAllFilters(handleClickClearAllFilters)
  onClickSearchByPhoneNumber(handleClickSearchByPhoneNumber)
  onClickSearchByEmail(handleClickSearchByEmail)
  onClickApproveRequest(handleClickApproveRequest)
  onClickDeclineRequest(handleClickDeclineRequest)
  onClickAllUsers(handleClickAllUsers)
  onClickPolicyModalLogin(handleClickOnPolicyModalLogin)
  onClickRequestPolicy(handleClickRequestPolicy)
  onClickRegDate(handleClickOnRegDate)
  onClickCancelRequest(handleClickCancelRequest)
  onClickSavePolicy(handleClickSavePolicy)
  onClickEditInfo(handleClickEditUser)
  onClickCreateAccount(handleClickCreateAccount)
  onClickUploadTable(handleClickUploadTable)
  onClickUpload(handleClickUpload)
  onClickFilterByRequestDate(handleClickFilterByRequestDate)
  onClickPrintPDF(handleClickPrintPDF)
  onClickSendPDFEmail(handleClickSendPDFEmail)
  onClickShow(handleClickShow)
  onClickShowInsTaxes(handleClickShowInsTaxes)
  onClickShowInsCoeff(handleClickShowInsCoeff)
  onClickNextPage(handleClickNextPage)
  onClickPreviousPage(handleClickPreviousPage)
  onClickPage(handleClickPage)
})