const getPartial = async (path) => {
  const source = await $.get(path)
  const partial = Handlebars.compile(source)

  return partial
}

const registerPartials = async () => {
  const [header, home, about, footer, register, login, offer,
    driver, requestCards, requestCardInfo, contact,
    myProfile, requestTable, offerDetails, carCard, carRow, requestTableAgent,
    userRow, allUsers, requestPolicy, uploadTable] = await Promise.all([
    getPartial('../views/partials/header.hbs'),
    getPartial('../views/partials/home.hbs'),
    getPartial('../views/partials/about.hbs'),
    getPartial('../views/partials/footer.hbs'),
    getPartial('../views/partials/register.hbs'),
    getPartial('../views/partials/login.hbs'),
    getPartial('../views/partials/offer.hbs'),
    getPartial('../views/partials/driver.hbs'),
    getPartial('../views/partials/requestCards.hbs'),
    getPartial('../views/partials/requestCardInfo.hbs'),
    getPartial('../views/partials/contact.hbs'),
    getPartial('../views/partials/myProfile.hbs'),
    getPartial('../views/partials/requestTable.hbs'),
    getPartial('../views/partials/offerDetails.hbs'),
    getPartial('../views/partials/car-card.hbs'),
    getPartial('../views/partials/car-row.hbs'),
    getPartial('../views/partials/requestTableAgent.hbs'),
    getPartial('../views/partials/user-row.hbs'),
    getPartial('../views/partials/allUsers.hbs'),
    getPartial('../views/partials/requestPolicy.hbs'),
    getPartial('../views/partials/uploadTable.hbs')
  ])

  await Promise.all([
    Handlebars.registerPartial('header', header),
    Handlebars.registerPartial('home', home),
    Handlebars.registerPartial('about', about),
    Handlebars.registerPartial('footer', footer),
    Handlebars.registerPartial('register', register),
    Handlebars.registerPartial('login', login),
    Handlebars.registerPartial('offer', offer),
    Handlebars.registerPartial('driver', driver),
    Handlebars.registerPartial('requestCards', requestCards),
    Handlebars.registerPartial('requestCardInfo', requestCardInfo),
    Handlebars.registerPartial('contact', contact),
    Handlebars.registerPartial('myProfile', myProfile),
    Handlebars.registerPartial('requestTable', requestTable),
    Handlebars.registerPartial('offerDetails', offerDetails),
    Handlebars.registerPartial('carCard', carCard),
    Handlebars.registerPartial('carRow', carRow),
    Handlebars.registerPartial('requestTableAgent', requestTableAgent),
    Handlebars.registerPartial('userRow', userRow),
    Handlebars.registerPartial('allUsers', allUsers),
    Handlebars.registerPartial('requestPolicy', requestPolicy),
    Handlebars.registerPartial('uploadTable', uploadTable)
  ])
}

export {
  registerPartials
}
