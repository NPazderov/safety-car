const getTemplateHtml = async (path, context) => {
  const source = await $.get(path)
  const template = Handlebars.compile(source)
  const html = template(context)

  return html
}

const viewHome = async (context) => {
  const homePageHtml = await getTemplateHtml('../../views/templates/homePage.hbs', context)
  $('.wrapper').empty().append(homePageHtml)
}

const viewAbout = async (context) => {
  const aboutPageHtml = await getTemplateHtml('../../views/templates/aboutPage.hbs', context)
  $('.wrapper').empty().append(aboutPageHtml)
}

const viewRegister = async (context) => {
  const registerPageHtml = await getTemplateHtml('../../views/templates/registerPage.hbs', context)
  $('.wrapper').empty().append(registerPageHtml)
}

const viewLogin = async (context) => {
  const loginPageHtml = await getTemplateHtml('../../views/templates/loginPage.hbs', context)
  $('.wrapper').empty().append(loginPageHtml)
}

const viewOffer = async (context) => {
  const offerPageHtml = await getTemplateHtml('../../views/templates/offerPage.hbs', context)
  $('.wrapper').empty().append(offerPageHtml)
}

const viewDriver = async (context) => {
  const driverPageHtml = await getTemplateHtml('../../views/templates/driverPage.hbs', context)
  $('.wrapper').empty().append(driverPageHtml)
}

const viewRequestCards = async (context) => {
  const requestCardsPageHtml = await getTemplateHtml('../../views/templates/requestCardsPage.hbs', context)
  $('.wrapper').empty().append(requestCardsPageHtml)
}

const viewRequestCardInfo = async (context) => {
  const requestCardInfoPageHtml = await getTemplateHtml('../../views/templates/requestCardInfoPage.hbs', context)
  $('.wrapper').empty().append(requestCardInfoPageHtml)
}

const viewContact = async (context) => {
  const contactPageHtml = await getTemplateHtml('../../views/templates/contactPage.hbs', context)
  $('.wrapper').empty().append(contactPageHtml)
}

const viewMyProfile = async (context) => {
  const myProfilePageHtml = await getTemplateHtml('../../views/templates/myProfilePage.hbs', context)
  $('.wrapper').empty().append(myProfilePageHtml)
}

const viewRequestTable = async (context) => {
  const requestTablePageHtml = await getTemplateHtml('../../views/templates/requestTablePage.hbs', context)
  $('.wrapper').empty().append(requestTablePageHtml)
}

const viewOfferDetails = async (context) => {
  const offerDetailsPageHtml = await getTemplateHtml('../../views/templates/offerDetailsPage.hbs', context)
  $('.wrapper').empty().append(offerDetailsPageHtml)
}

const viewRequestTableAgent = async (context) => {
  const requestTableAgentPage = await getTemplateHtml('../../views/templates/requestTableAgentPage.hbs', context)
  $('.wrapper').empty().append(requestTableAgentPage)
}

const viewAllUsers = async (context) => {
  const allUsersPageHtml = await getTemplateHtml('../../views/templates/allUsersPage.hbs', context)
  $('.wrapper').empty().append(allUsersPageHtml)
}

const viewRequestPolicy = async (context) => {
  const requestPolicyPageHtml = await getTemplateHtml('../../views/templates/requestPolicyPage.hbs', context)
  $('.wrapper').empty().append(requestPolicyPageHtml)
}

const viewUploadTable = async (context) => {
  const uploadTablePageHtml = await getTemplateHtml('../../views/templates/uploadTablePage.hbs', context)
  $('.wrapper').empty().append(uploadTablePageHtml)
}
export {
  viewHome,
  viewAbout,
  viewRegister,
  viewLogin,
  viewOffer,
  viewDriver,
  viewRequestCards,
  viewRequestCardInfo,
  viewContact,
  viewMyProfile,
  viewOfferDetails,
  viewRequestTable,
  viewRequestTableAgent,
  viewAllUsers,
  viewRequestPolicy,
  viewUploadTable
}