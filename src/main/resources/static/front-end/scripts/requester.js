const baseUrl = 'http://localhost:8080/api'

const postLogin = (userData) => {
  return $.ajax({
    url: `${baseUrl}/auth/login`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(userData)
  })
}

const postRegister = (userData) => {
  return $.ajax({
    url: `${baseUrl}/auth/signup`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(userData)
  })
}

const loadAllCars = (cars) => {
  const distinctCars = [...new Set(cars.map(x => x.make))]
  distinctCars.forEach((car) => {
    $('#inputMake').append(
      $('<option id="makeOption">').text(car)
    )
  })
}

const getAllCars = async () => {
  const cars = await fetch(`${baseUrl}/getQuote/carDetail`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })

  const allCars = cars.json()
  return allCars
}

const loadMake = () => {
  getAllCars()
    .then((cars) => loadAllCars(cars))
}

const loadModels = (cars) => {
  const distinctModels = [...new Set(cars.map(x => x.model))]
  $('#inputModel').empty()
  $('#inputModel').append('<option> Choose...')
  distinctModels.forEach((car) => {
    $('#inputModel').append(
      $('<option id="modelOption">').text(car)
    )
  })
}

const getModels = (make) => {
  $.get({
      url: `${baseUrl}/getQuote/carModels?make=${make}`,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then((cars) => loadModels(cars))
    .catch((error) => console.log(error))
}

const loadCC = (cars) => {
  const distinctCars = [...new Set(cars.map(x => x.cubicCapacity))]
  $('#inputCC').empty()
  $('#inputCC').append('<option> Choose...')
  distinctCars.forEach((car) => {
    $('#inputCC').append(
      $('<option id="CCOption">').text(car)
    )
  })
}

const getCC = (model) => {
  $.get({
      url: `${baseUrl}/getQuote/carCC?model=${model}`,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then((cars) => loadCC(cars))
    .catch((error) => console.log(error))
}

const getUniqueCar = async (make, model, cubicCapacity) => {
  const cars = await getAllCars()

  let uniqueCar = cars.filter(function (car) {
    return car.make === make
  })
  uniqueCar = uniqueCar.filter(function (car) {
    return car.model === model
  })
  uniqueCar = uniqueCar.filter(function (car) {
    return +car.cubicCapacity === +cubicCapacity
  })

  return uniqueCar
}

const getMakeOption = () => {
  const selectedMake = $('#inputMake').find(':selected')
  sessionStorage.setItem('make', selectedMake.text())
  return selectedMake
}

const getModelOption = () => {
  const selectedModel = $('#inputModel').find(':selected')
  sessionStorage.setItem('model', selectedModel.text())
  return selectedModel
}

const getCCOption = () => {
  const selectedCC = $('#inputCC').find(':selected')
  sessionStorage.setItem('cubicCapacity', selectedCC.text())
}

const getRegDate = () => {
  const datePicker = $('#regDate').datepicker().val()
  sessionStorage.setItem('firstRegistrationDate', datePicker)
}

const getBdate = () => {
  const datePicker = $('#bDate').datepicker().val()
  sessionStorage.setItem('dateOfBirth', datePicker)
}

const getNoAccidents = () => {
  const selectedRadio = $('#no').val()
  sessionStorage.setItem('accidents', selectedRadio)
}

const getYesAccidents = () => {
  const selectedRadio = $('#yes').val()
  sessionStorage.setItem('accidents', selectedRadio)
}

const getUserRequests = async (email, token) => {
  const userRequests = await $.get({
    crossDomain: true,
    url: `${baseUrl}/users/requests?email=${email}`,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    }
  })

  return userRequests
}

const getMe = async (token) => {
  const user = await fetch(`${baseUrl}/auth/me`, {
    crossDomain: true,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    }
  })

  const currUser = user.json()

  return currUser
}

const getFirstName = () => {
  const userName = sessionStorage.getItem('firstName')
  return userName
}

const getTotalPremium = async (carData) => {
  const premium = await fetch(`${baseUrl}/getQuote/result`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(carData)
  })
  const totalPremium = premium.json()
  return totalPremium
}

const getPolicyById = async (id, userRequests) => {
  const policyById = getUserRequests(userRequests).find(x => userRequests.id === id)
  console.log(id)
  return policyById
}

const getDetails = () => {
  const details = {
    make: sessionStorage.getItem('make'),
    model: sessionStorage.getItem('model'),
    cubicCapacity: sessionStorage.getItem('cubicCapacity'),
    firstRegistrationDate: sessionStorage.getItem('firstRegistrationDate'),
    accidents: sessionStorage.getItem('accidents'),
    dateOfBirth: sessionStorage.getItem('dateOfBirth'),
    totalPremium: sessionStorage.getItem('totalPremium')
  }
  return details
}

const getToken = () => {
  const token = sessionStorage.getItem('authToken')
  return token
}

const getMail = () => {
  const email = sessionStorage.getItem('email')
  return email
}

const getUser = () => {
  const authToken = getToken()
  const email = getMail()
  const firstName = getFirstName()
  const user = {
    authToken,
    email,
    firstName
  }

  if (user.authToken !== null) {
    user.isLoggedIn = true
  }

  const token = sessionStorage.getItem('authToken')
  const jwtData = token.split('.')[1]
  const decodedJwtJsonData = window.atob(jwtData)
  const decodedJwtData = JSON.parse(decodedJwtJsonData)

  const role = decodedJwtData.role

  if (role === 'ROLE_CLIENT') {
    user.isClient = true
  }
  if (role === 'ROLE_ADMIN') {
    user.isAdmin = true
  }
  if (role === 'ROLE_AGENT') {
    user.isAgent = true
  }

  return user
}

const logout = () => {
  sessionStorage.clear()
}

const getRole = (currData) => {
  const token = sessionStorage.getItem('authToken')
  const jwtData = token.split('.')[1]
  const decodedJwtJsonData = window.atob(jwtData)
  const decodedJwtData = JSON.parse(decodedJwtJsonData)

  const role = decodedJwtData.role

  if (role === 'ROLE_CLIENT') {
    currData.isClient = true
  }
  if (role === 'ROLE_ADMIN') {
    currData.isAdmin = true
  }
  if (role === 'ROLE_AGENT') {
    currData.isAgent = true
  }

  return currData
}

const getAllPendingRequests = async (token, pageNumber) => {
  const response = await fetch(`${baseUrl}/allRequests/byStatus?status=PENDING&page=${pageNumber}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })

  const allPendingRequests = response.json()

  return allPendingRequests
}

const getAllApprovedRequests = async (token, pageNumber) => {
  const response = await fetch(`${baseUrl}/allRequests/byStatus?status=APPROVED&page=${pageNumber}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })

  const getAllApprovedRequests = response.json()

  return getAllApprovedRequests
}

const getAllDeclinedRequests = async (token, pageNumber) => {
  const response = await fetch(`${baseUrl}/allRequests/byStatus?status=DECLINED&page=${pageNumber}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })

  const allDeclinedRequests = response.json()

  return allDeclinedRequests
}

const getAllRequests = async (token, pageNumber) => {
  const response = await fetch(`${baseUrl}/allRequests?page=${pageNumber}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })

  const allRequests = response.json()

  return allRequests
}

const searchByPhoneNumber = async (number, token) => {
  const requests = await fetch(`${baseUrl}/allRequests/findByPhoneNumber?phoneNumber=${number}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })

  const allRequests = requests.json()

  return allRequests
}

const searchByEmail = async (email, token) => {
  const requests = await fetch(`${baseUrl}/allRequests/findByEmail?email=${email}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })

  const allRequests = requests.json()

  return allRequests
}

const setRequestApproved = async (policyId, token) => {
  const requests = await fetch(`${baseUrl}/allRequests/approve/${policyId}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
  const currentPolicy = requests.json()

  console.log(currentPolicy)

  return currentPolicy
}

const setRequestDeclined = async (policyId, token) => {
  const requests = await fetch(`${baseUrl}/allRequests/decline/${policyId}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
  const currentPolicy = requests.json()

  return currentPolicy
}

const getAllUsers = async (token) => {
  const users = await fetch(`${baseUrl}/users/getAll`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })

  const allUsers = users.json()

  return allUsers
}

const setCancelRequest = async (requestId) => {
  const token = getToken()
  const user = await getMe(token)
  const userId = user.id
  console.log(userId)
  const request = await fetch(`${baseUrl}/users/myRequests/cancel/${userId}?requestId=${requestId}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
  // const currentPolicy = request.json()

  return request
}

const savePolicy = async (form) => {

  const token = getToken()
  const policy = await fetch(`${baseUrl}/applications/new`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: form
  })

  const currPolicy = await policy.json()

  return currPolicy
}

// Handlebars.registerHelper('inc', function (value, options) {
//   return parseInt(value) + 1
// })

const editUser = async (userData) => {
  const token = getToken()
  const user = await getMe(token)
  return $.ajax({
    url: `${baseUrl}/users/${user.id}`,
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    },
    data: JSON.stringify(userData)
  })
}

const uploadTable = async (form) => {
  const token = getToken()
  const table = await fetch(`${baseUrl}/upload/table`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`
    },
    body: form
  })

  const currTable = await table.json()

  return currTable
}

const filterFromDateToDate = async (fromDate, toDate) => {
  const token = getToken()
  const requests = await fetch(`${baseUrl}/allRequests/findByRequestDate?fromDate=${fromDate}&toDate=${toDate}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })

  const allRequests = requests.json()

  return allRequests
}

const getPDF = () => {
  const id = $('#rciID').text()
  const spd = $('#rciStartPolicyDate').text()
  const carmake = $('#rciCarMake').text()
  const model = $('#rciCarModel').text()
  const cc = $('#rciCarCubicCapacity').text()
  const frd = $('#rciFirstRegistrationDate').text()
  const names = $('#rciUserNames').text()
  const driverAge = $('#rciDriverAge').text()
  const accidents = $('#rciAccidents').text()
  const phone = $('#rciPhone').text()
  const email = $('#rciEmail').text()
  const town = $('#rciTown').text()
  const postalCode = $('#rciPostalCode').text()
  const address = $('#rciAddress').text()
  const premium = $('#rciPremium').text()

  html2canvas(document.body)
    .then(function (canvas) {
      // const img = canvas.toDataURL("image/png")
      const doc = new jsPDF()

      doc.setFont('helvetica').setFontType('bold')
      doc.text('Safety Car Policy', 50, 10);
      doc.setFont('helvetica')
      doc.text(`Request id: ${id}`, 10, 20);
      doc.text(`Start Policy Date: ${spd}`, 10, 30);

      doc.setFont('helvetica').setFontType('bold')
      doc.text(`Car details:`, 10, 50);

      doc.setFont('helvetica')
      doc.text(`${carmake}`, 15, 60)
      doc.text(`${model}`, 15, 70)
      doc.text(`${cc}`, 15, 80)
      doc.text(`${frd}`, 15, 90);

      doc.setFont('helvetica').setFontType('bold')
      doc.text(`User Details:`, 10, 110);

      doc.setFont('helvetica')
      doc.text(`${names}`, 15, 120)
      doc.text(`${driverAge}`, 15, 130)
      doc.text(`${accidents}`, 15, 140);


      doc.text(`Contacts:`, 10, 160).setFont('helvetica').setFontType('bold')
      doc.text(`${phone}`, 15, 170)
      doc.text(`${email}`, 15, 180)
      doc.text(`${town}`, 15, 190)
      doc.text(`${postalCode}`, 15, 200)
      doc.text(`${address}`, 15, 210)
      doc.text(`Total Premium: ${premium}`, 50, 240).setFont('helvetica').setFontType('bold').setFontSize(25)


      doc.save('test.pdf')
    })
}

const generatePDF = async () => {
  const token = getToken()
  const policyId = sessionStorage.getItem('policyId')
  const user = await getMe(token)
  $.ajax({
    url: `${baseUrl}/users/myRequests/printPreviewPolicy/${user.id}?requestId=${policyId}`,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    },
    xhrFields: {
      responseType: 'blob'
    },
    success: function (blob) {
      console.log(blob.size)
      const link = document.createElement('a')
      link.href = window.URL.createObjectURL(blob)
      link.click()
      window.open(link)
    }
  })
}

const sendPDFEmail = async () => {
  const token = getToken()
  const id = sessionStorage.getItem('policyId')
  const user = await getMe(token)
  await fetch(`${baseUrl}/users/sendPDFEmail?policyId=${id}&userId=${user.id}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
}

const showTable = async (token) => {
  const data = await fetch(`${baseUrl}/upload`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })

  const currData = data.json()
  return currData
}

const showInsTaxes = async (token) => {
  const data = await fetch(`${baseUrl}/upload/getTaxes`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
  const currData = data.json()
  return currData
}

const showInsCoeff = async (token) => {
  const data = await fetch(`${baseUrl}/upload/getCoefficients`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
  const currData = data.json()
  return currData
}

export {
  postLogin,
  postRegister,
  getAllCars,
  getModels,
  getCC,
  getMakeOption,
  getModelOption,
  getCCOption,
  getRegDate,
  getBdate,
  getNoAccidents,
  getYesAccidents,
  getUserRequests,
  getPolicyById,
  getDetails,
  getTotalPremium,
  getUser,
  getToken,
  getMail,
  getMe,
  logout,
  getFirstName,
  getRole,
  getAllPendingRequests,
  getAllApprovedRequests,
  getAllDeclinedRequests,
  getAllRequests,
  searchByPhoneNumber,
  searchByEmail,
  setRequestApproved,
  setRequestDeclined,
  getAllUsers,
  getUniqueCar,
  loadMake,
  setCancelRequest,
  savePolicy,
  editUser,
  uploadTable,
  filterFromDateToDate,
  getPDF,
  generatePDF,
  sendPDFEmail,
  showTable,
  showInsTaxes,
  showInsCoeff
}