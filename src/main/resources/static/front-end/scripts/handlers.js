import {
  viewHome,
  viewAbout,
  viewRegister,
  viewLogin,
  viewOffer,
  viewDriver,
  viewRequestCards,
  viewRequestCardInfo,
  viewContact,
  viewMyProfile,
  viewRequestTable,
  viewOfferDetails,
  viewRequestTableAgent,
  viewAllUsers,
  viewRequestPolicy,
  viewUploadTable
} from './view.js'

import {
  postLogin,
  postRegister,
  getModels,
  getMakeOption,
  getModelOption,
  getCC,
  getCCOption,
  getRegDate,
  getBdate,
  getNoAccidents,
  getYesAccidents,
  getUserRequests,
  getDetails,
  getTotalPremium,
  getUser,
  logout,
  getAllPendingRequests,
  getAllApprovedRequests,
  getAllDeclinedRequests,
  getAllRequests,
  searchByPhoneNumber,
  searchByEmail,
  setRequestApproved,
  setRequestDeclined,
  getToken,
  getMe,
  getAllUsers,
  getUniqueCar,
  loadMake,
  setCancelRequest,
  savePolicy,
  editUser,
  getRole,
  uploadTable,
  filterFromDateToDate,
  generatePDF,
  sendPDFEmail,
  showTable,
  showInsTaxes,
  showInsCoeff
} from './requester.js'

const handleOnLoadHome = () => {
  if (getToken() !== null) {
    viewHome(getUser())
  } else {
    viewHome()
  }
}

const handleClickHome = () => {
  if (getToken() !== null) {
    viewHome(getUser())
  } else {
    viewHome()
  }
}

const handleClickAbout = () => {
  if (getToken() !== null) {
    viewAbout(getUser())
  } else {
    viewAbout()
  }
}

const handleClickRegister = () => {
  viewRegister()
}

const handleClickLogin = () => {
  viewLogin()
}

const handleClickLoginButton = async () => {
  try {
    const email = $('#email').val()
    const password = $('#password').val()

    const authToken = await postLogin({
      email,
      password
    })
    sessionStorage.setItem('authToken', authToken)
    sessionStorage.setItem('email', email)
    // sessionStorage.setItem('welcomeEmail', email.substring(0, email.indexOf('@')))

    const user = await getMe(getToken())
    sessionStorage.setItem('firstName', user.firstName)

    viewHome(getUser())
  } catch (err) {
    $('#loginError').append(`<div class="alert alert-danger" role="alert">
    Wrong email or password. Please try again
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">
        <i class="now-ui-icons ui-1_simple-remove"></i>
      </span>
    </button>
  </div>`)
  }
}

const handleClickOffer = async () => {
  const accidents = sessionStorage.getItem('accidents')
  const make = sessionStorage.getItem('make')
  const model = sessionStorage.getItem('model')
  const cubicCapacity = sessionStorage.getItem('cubicCapacity')
  const dateOfBirth = sessionStorage.getItem('dateOfBirth')
  const firstRegistrationDate = sessionStorage.getItem('firstRegistrationDate')
  const totalPremium = sessionStorage.getItem('totalPremium')
  const monthlyPremium = sessionStorage.getItem('monthlyPremium')

  if (accidents !== null && make !== null && model !== null && cubicCapacity !== null && dateOfBirth !== null &&
    firstRegistrationDate !== null && totalPremium !== null && monthlyPremium !== null) {
    const {
      isLoggedIn,
      firstName
    } = getUser()
    viewOfferDetails({
      make,
      model,
      cubicCapacity,
      firstRegistrationDate,
      accidents,
      dateOfBirth,
      totalPremium,
      firstName,
      monthlyPremium,
      isLoggedIn
    })
  } else {
    loadMake()
    if (getToken() !== null) {
      viewOffer(getUser())
    } else {
      viewOffer()
    }
  }
}

const handleClickGetStarted = async () => {
  try {
    const firstName = $('#firstName').val()

    if (firstName === '' && !/^[A-Za-z]+$/.test(firstName)) {
      $('#firstNameValidation').append(`<div class="form-group has-danger">
      <input type="email" value="Please enter valid firstName" class="form-control form-control-danger" readonly>
    </div>`)
      return
    }

    const lastName = $('#lastName').val()

    if (lastName === '' && !/^[A-Za-z]+$/.test(lastName)) {
      $('#lastNameValidation').append(`<div class="form-group has-danger">
      <input type="email" value="Please enter valid last name" class="form-control form-control-danger" readonly>
    </div>`)
      return
    }

    const email = $('#regEmail').val()

    if (email === '' || !email.includes('@') || !email.includes('.')) {
      $('#regEmailValidation').append(`<div class="form-group has-danger">
      <input type="email" value="Please enter valid email" class="form-control form-control-danger" readonly>
    </div>`)
      return
    }

    const password = $('#regPassword').val()
    if (password === '' || !/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,20}$/.test(password)) {
      $('#regPasswordValidation').append(`<div class="form-group has-danger">
      <input type="email" value="Password should contains A-z 0-9" class="form-control form-control-danger" readonly>
    </div>`)
      return
    }

    const repPassword = $('#regPasswordRepeat').val()
    if (repPassword === '' && repPassword !== password) {
      $('#regPasswordRepeatValidation').append(`<div class="form-group has-danger">
      <input type="email" value="Passwords dont match" class="form-control form-control-danger" readonly>
    </div>`)
      return
    }

    if (password !== repPassword) {
      alert('Passwords dont match, try again. I love you')
    }

    await postRegister({
      email,
      password,
      firstName,
      lastName
    })

    sessionStorage.setItem('email', email)
    sessionStorage.setItem('firstName', firstName)
    sessionStorage.setItem('lastName', lastName)

    viewHome()
  } catch (err) {
    alert('This email is already is use!')
  }
}

const handleClickGetPrice = async () => {
  getBdate()

  const make = sessionStorage.getItem('make')
  const model = sessionStorage.getItem('model')
  const cubicCapacity = sessionStorage.getItem('cubicCapacity')
  const car = await getUniqueCar(make, model, cubicCapacity)
  const carData = {
    car: car[0],
    firstRegistrationDate: sessionStorage.getItem('firstRegistrationDate'),
    dateOfBirth: sessionStorage.getItem('dateOfBirth'),
    accidents: sessionStorage.getItem('accidents')
  }

  const totalPremium = await getTotalPremium(carData)
  const monthlyPremium = (totalPremium / 12).toFixed(2)
  sessionStorage.setItem('monthlyPremium', monthlyPremium)
  sessionStorage.setItem('totalPremium', totalPremium.toFixed(2))

  const {
    firstRegistrationDate,
    accidents,
    dateOfBirth
  } = getDetails()

  if (getToken()) {
    const {
      isLoggedIn,
      firstName
    } = getUser()
    viewOfferDetails({
      make,
      model,
      cubicCapacity,
      firstRegistrationDate,
      accidents,
      dateOfBirth,
      totalPremium: sessionStorage.getItem('totalPremium'),
      firstName,
      monthlyPremium,
      isLoggedIn
    })
  } else {
    viewOfferDetails({
      make,
      model,
      cubicCapacity,
      firstRegistrationDate,
      accidents,
      dateOfBirth,
      totalPremium: sessionStorage.getItem('totalPremium'),
      monthlyPremium
    })
  }
}

const handleClickMakeOption = () => {
  getMakeOption()
  const make = getMakeOption().text()
  getModels(make)
}

const handleClickModelOption = () => {
  getModelOption()
  const model = getModelOption().text()
  getCC(model)
}

const handleClickOnRegDate = () => {
  getCCOption()
}

const handleClickDriverInfo = async () => {
  getRegDate()

  // const datePicker = String($('#regDate').datepicker().val())
  // let today = new Date()
  // const dd = today.getDate()
  // const mm = today.getMonth() + 1
  // const yyyy = today.getFullYear()

  // today = mm + '-' + dd + '-' + yyyy

  // if (yyyy < datePicker.substr(0, 4)) {
  //   $('#regDateError').append(`<div class="alert alert-primary" role="alert">
  //    Date cannot be in the future.
  // </div>`
  //   )
  //   return
  // }

  const make = sessionStorage.getItem('make')
  const model = sessionStorage.getItem('model')
  const cubicCapacity = sessionStorage.getItem('cubicCapacity')

  const selectedMake = $('#inputMake').find(':selected')
  const selectedModel = $('#inputModel').find(':selected')
  const selectedCC = $('#inputCC').find(':selected')
  if (selectedMake.val() === 'Choose...' || selectedModel.val() === 'Choose...' ||
    selectedCC.val() === 'Choose...') {
    $('#carDetailsError').append(`<div class="alert alert-primary" role="alert">
      All field are required.
    </div>`)
    return
  }

  console.log(await getUniqueCar(make, model, cubicCapacity))

  if (getToken() !== null) {
    viewDriver(getUser())
  } else {
    viewDriver()
  }
}

const handleClickMyRequests = async () => {
  const email = sessionStorage.getItem('email')
  const token = sessionStorage.getItem('authToken')
  let userRequests = await getUserRequests(email, token)
  userRequests = userRequests.map(request => {
    request.isPending = request.policyState === 'PENDING'
    request.isApproved = request.policyState === 'APPROVED'
    request.isDeclined = request.policyState === 'DECLINED'

    return request
  })

  const {
    isLoggedIn,
    firstName
  } = getUser()

  // console.log(userRequests)
  viewRequestCards({
    cards: userRequests,
    firstName,
    isLoggedIn
  })
}

const handleClickRequestCardInfo = async (event) => {
  const {
    id
  } = event.target
  const email = sessionStorage.getItem('email')
  const token = sessionStorage.getItem('authToken')
  sessionStorage.setItem('policyId', id)
  let requests
  if (getUser().isAdmin || getUser().isAgent) {
    requests = await getAllRequests(token, 0)
    const currentPolicy = requests.content.find(r => +r.id === +id)
    if (currentPolicy.policyState === 'PENDING') {
      currentPolicy.isPending = true
    }

    if (currentPolicy.policyState === 'APPROVED') {
      currentPolicy.isApproved = true
    }
    if (currentPolicy.policyState === 'DECLINED') {
      currentPolicy.isDeclined = true
    }
    getRole(currentPolicy)
    const {
      isLoggedIn,
      firstName
    } = getUser()

    currentPolicy.isLoggedIn = isLoggedIn
    currentPolicy.firstName = firstName

    viewRequestCardInfo(currentPolicy)
  } else {
    requests = await getUserRequests(email, token)
    const currentPolicy = requests.find(r => +r.id === +id)
    console.log(currentPolicy)
    if (currentPolicy.policyState === 'PENDING') {
      currentPolicy.isPending = true
    }

    if (currentPolicy.policyState === 'APPROVED') {
      currentPolicy.isApproved = true
    }
    if (currentPolicy.policyState === 'DECLINED') {
      currentPolicy.isDeclined = true
    }
    getRole(currentPolicy)
    const {
      isLoggedIn,
      firstName
    } = getUser()

    currentPolicy.isLoggedIn = isLoggedIn
    currentPolicy.firstName = firstName

    viewRequestCardInfo(currentPolicy)
  }
}

const handleClickContact = () => {
  if (getToken()) {
    viewContact(getUser())
  } else {
    viewContact()
  }
}

const handleClickMyProfile = async () => {
  const {
    firstName,
    lastName,
    email
  } = await getMe(getToken())

  const {
    isLoggedIn
  } = getUser()
  viewMyProfile({
    isLoggedIn,
    firstName,
    lastName,
    email
  })
}

const handleClickRequestTable = async () => {
  const email = sessionStorage.getItem('email')
  const token = sessionStorage.getItem('authToken')
  let userRequests = await getUserRequests(email, token)
  userRequests = userRequests.map(request => {
    request.isPending = request.policyState === 'PENDING'
    request.isApproved = request.policyState === 'APPROVED'
    request.isDeclined = request.policyState === 'DECLINED'

    return request
  })
  const {
    isLoggedIn,
    firstName
  } = getUser()

  viewRequestTable({
    rows: userRequests,
    firstName,
    isLoggedIn
  })
}

const handleClickOfferBack = () => {
  if (getToken()) {
    viewHome(getUser())
  } else {
    viewHome()
  }
}

const handleClickDriverBack = () => {
  if (getToken()) {
    viewOffer(getUser())
  } else {
    viewOffer()
  }
}

const handleClickODBack = () => {
  if (getToken()) {
    viewDriver(getUser())
  } else {
    viewDriver()
  }
}

const handleClickNo = () => {
  getNoAccidents()
}

const handleClickYes = () => {
  getYesAccidents()
}

const handleClickLogout = () => {
  logout()
  viewHome()
}

const handleClickAllPendingRequests = async () => {
  const token = sessionStorage.getItem('authToken')
  sessionStorage.setItem('requests', 'pending')
  let pendingRequests = await getAllPendingRequests(token, 0)
  pendingRequests = pendingRequests.content.map(request => {
    request.isPending = request.policyState === 'PENDING'
    return request
  })
  const {
    isLoggedIn,
    firstName,
    isAdmin,
    isAgent
  } = getUser()

  viewRequestTableAgent({
    rows: pendingRequests,
    firstName,
    isLoggedIn,
    isAdmin,
    isAgent,
    filter: true
  })
}

const handleClickAllApprovedRequests = async () => {
  const token = sessionStorage.getItem('authToken')
  sessionStorage.setItem('requests', 'approved')
  let approvedRequests = await getAllApprovedRequests(token, 0)
  approvedRequests = approvedRequests.content.map(request => {
    request.isApproved = request.policyState === 'APPROVED'
    return request
  })
  const {
    isLoggedIn,
    firstName,
    isAdmin,
    isAgent
  } = getUser()

  viewRequestTableAgent({
    rows: approvedRequests,
    firstName,
    isLoggedIn,
    isAdmin,
    isAgent,
    filter: true
  })
}

const handleClickAllDeclinedRequests = async () => {
  const token = sessionStorage.getItem('authToken')
  sessionStorage.setItem('requests', 'declined')
  let declinedRequests = await getAllDeclinedRequests(token, 0)
  declinedRequests = declinedRequests.content.map(request => {
    request.isDeclined = request.policyState === 'DECLINED'
    return request
  })

  const {
    isLoggedIn,
    firstName,
    isAdmin,
    isAgent
  } = getUser()

  viewRequestTableAgent({
    rows: declinedRequests,
    firstName,
    isLoggedIn,
    isAdmin,
    isAgent,
    filter: true
  })
}

const handleClickAllRequests = async () => {
  sessionStorage.setItem('requests', 'all')
  const token = sessionStorage.getItem('authToken')
  let allRequests = await getAllRequests(token, 0)
  allRequests = allRequests.content.map(request => {
    request.isPending = request.policyState === 'PENDING'
    request.isApproved = request.policyState === 'APPROVED'
    request.isDeclined = request.policyState === 'DECLINED'
    return request
  })

  const {
    isLoggedIn,
    firstName,
    isAdmin,
    isAgent
  } = getUser()

  viewRequestTableAgent({
    rows: allRequests,
    firstName,
    isLoggedIn,
    isAdmin,
    isAgent,
    filter: true
  })
}

const handleClickAllUsers = async () => {
  const token = getToken()
  const users = await getAllUsers(token)
  const {
    isLoggedIn,
    firstName,
    isAdmin,
    isAgent
  } = getUser()

  viewAllUsers({
    rows: users,
    firstName,
    isLoggedIn,
    isAdmin,
    isAgent
  })
}

const handleClickFilterPage = () => {
  viewRequestTableAgent(getUser())
}

const handleClickClearAllFilters = () => {
  viewRequestTableAgent(getUser())
}

const handleClickSearchByPhoneNumber = async () => {
  const token = sessionStorage.getItem('authToken')
  const number = $('#phoneNumber').val()
  let allRequestsByPhone = await searchByPhoneNumber(number, token)
  allRequestsByPhone = allRequestsByPhone.map(request => {
    request.isPending = request.policyState === 'PENDING'
    request.isApproved = request.policyState === 'APPROVED'
    request.isDeclined = request.policyState === 'DECLINED'
    return request
  })

  const {
    isLoggedIn,
    firstName,
    isAdmin,
    isAgent
  } = getUser()

  viewRequestTableAgent({
    rows: allRequestsByPhone,
    firstName,
    isLoggedIn,
    isAdmin,
    isAgent
  })
}

const handleClickSearchByEmail = async () => {
  const token = sessionStorage.getItem('authToken')
  const email = $('#email').val()
  let allRequestsByEmail = await searchByEmail(email, token)
  allRequestsByEmail = allRequestsByEmail.map(request => {
    request.isPending = request.policyState === 'PENDING'
    request.isApproved = request.policyState === 'APPROVED'
    request.isDeclined = request.policyState === 'DECLINED'
    return request
  })
  const {
    isLoggedIn,
    firstName,
    isAdmin,
    isAgent
  } = getUser()

  viewRequestTableAgent({
    rows: allRequestsByEmail,
    firstName,
    isLoggedIn,
    isAdmin,
    isAgent
  })
}

const handleClickApproveRequest = async () => {
  const policyId = sessionStorage.getItem('policyId')
  const newPolicy = await setRequestApproved(policyId, getToken())
  if (newPolicy.policyState === 'APPROVED') {
    newPolicy.isApproved = true
  }
  const {
    isLoggedIn,
    firstName,
    isAdmin
  } = getUser()
  newPolicy.isLoggedIn = isLoggedIn
  newPolicy.firstName = firstName
  newPolicy.isAdmin = isAdmin
  viewRequestCardInfo(newPolicy)
}

const handleClickDeclineRequest = async () => {
  const id = sessionStorage.getItem('policyId')
  const newPolicy = await setRequestDeclined(id, getToken())
  if (newPolicy.policyState === 'DECLINED') {
    newPolicy.isDeclined = true
  }
  const {
    isLoggedIn,
    firstName,
    isAdmin
  } = getUser()
  newPolicy.isLoggedIn = isLoggedIn
  newPolicy.firstName = firstName
  newPolicy.isAdmin = isAdmin
  viewRequestCardInfo(newPolicy)
}
// Handlebars.registerHelper('inc', function (value, options) {
//   return parseInt(value) + 1
// })

const handleClickOnPolicyModalLogin = () => {
  viewLogin()
}

const handleClickRequestPolicy = () => {
  const totalPremium = sessionStorage.getItem('totalPremium')
  const monthlyPremium = sessionStorage.getItem('monthlyPremium')
  const {
    isLoggedIn,
    firstName,
    isClient,
    isAgent,
    isAdmin
  } = getUser()

  viewRequestPolicy({
    firstName,
    isLoggedIn,
    isClient,
    isAgent,
    isAdmin,
    totalPremium,
    monthlyPremium
  })
}

const handleClickCancelRequest = async () => {
  const policyId = sessionStorage.getItem('policyId')
  const unactivePolicy = await setCancelRequest(policyId)
  if (unactivePolicy.policyState === 'UNACTIVE') {
    unactivePolicy.isCanceled = true
  }

  const email = sessionStorage.getItem('email')
  const token = sessionStorage.getItem('authToken')
  let userRequests = await getUserRequests(email, token)
  userRequests = userRequests.map(request => {
    request.isPending = request.policyState === 'PENDING'
    request.isApproved = request.policyState === 'APPROVED'
    request.isDeclined = request.policyState === 'DECLINED'

    return request
  })

  const {
    isLoggedIn,
    firstName
  } = getUser()

  viewRequestCards({
    cards: userRequests,
    isLoggedIn,
    firstName
  })
}

const handleClickSavePolicy = async () => {
  const details = getDetails()
  const make = details.make
  const model = details.model
  const cubicCapacity = details.cubicCapacity
  const uniqueCar = await getUniqueCar(make, model, cubicCapacity)
  const image = $('#image')[0].files[0]
  const form = new FormData()
  form.append('carCertificateImage', image)
  form.append('premium', details.totalPremium)
  form.append('firstRegistrationDate', details.firstRegistrationDate)
  form.append('dateOfBirth', details.dateOfBirth)
  form.append('accidents', details.accidents)
  form.append('startPolicyDate', $('#startPolicyDate').datepicker().val())
  form.append('policyEmail', $('#policyEmail').val())
  form.append('town', $('#policyTown').val())
  form.append('postalCode', $('#policyPostalCode').val())
  form.append('phone', $('#policyPhone').val())
  form.append('address', $('#policyAddress').val())
  form.append('firstName', $('#policyFirstName').val())
  form.append('lastName', $('#policyLastName').val())
  form.append('car.make', make)
  form.append('car.model', model)
  form.append('car.cubicCapacity', cubicCapacity)
  form.append('car.id', uniqueCar[0].id)

  await savePolicy(form)
  sessionStorage.removeItem('make')
  handleClickMyRequests()
}

const handleClickUpload = async () => {
  const table = $('#table')[0].files[0]
  console.log(table)
  const form = new FormData()
  form.append('file', table)

  $('#ok').append(`<div class="alert alert-success"  width="25%" role="alert">
  <div class="container  width="25%" ">
    <div class="alert-icon  width="25%"">
      <i class="now-ui-icons ui-2_like"></i>
    </div>
    <strong>Well done!</strong> You successfully added a table. You are the best admin!
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">
        <i class="now-ui-icons ui-1_simple-remove"></i>
      </span>
    </button>
  </div>
</div>`)

  await uploadTable(form)
}

const handleClickEditUser = async () => {
  const email = $('#emailChange').val()
  const firstName = $('#firstNameChange').val()
  const lastName = $('#lastNameChange').val()
  const password = $('#passwordChange').val()
  const repPassword = $('#repPasswordChange').val()

  if (password === '') {
    alert('Please Enter Password')
    return
  }
  if (repPassword === '') {
    alert('Please Enter Confirmation Password')
    return
  }
  if (password !== repPassword) {
    alert('Passwords dont match, try again. I love you')
    return
  }

  const token = getToken()
  const user = await getMe(token)
  const userData = {
    id: user.id,
    email,
    firstName,
    lastName,
    password,
    role: user.role
  }
  editUser(userData)
  logout()
  viewLogin()
}

const handleClickCreateAccount = () => {
  viewRegister()
}

const handleClickUploadTable = () => {
  const {
    isLoggedIn,
    firstName,
    isAdmin,
    isAgent
  } = getUser()

  viewUploadTable({
    firstName,
    isLoggedIn,
    isAdmin,
    isAgent
  })
}

const handleClickFilterByRequestDate = async () => {
  let today = new Date()
  const dd = String(today.getDate()).padStart(2, '0')
  const mm = String(today.getMonth() + 1).padStart(2, '0')
  const yyyy = today.getFullYear()

  today = yyyy + '-' + mm + '-' + dd

  let fromDate = $('#fromDate').datepicker().val()
  let toDate = $('#toDate').datepicker().val()
  if (toDate === '') {
    toDate = today
  }
  if (fromDate === '') {
    fromDate = '1000-01-01'
  }
  let filteredRequests = await filterFromDateToDate(fromDate, toDate)
  filteredRequests = filteredRequests.map(request => {
    request.isPending = request.policyState === 'PENDING'
    request.isApproved = request.policyState === 'APPROVED'
    request.isDeclined = request.policyState === 'DECLINED'
    return request
  })

  const {
    isLoggedIn,
    firstName,
    isAdmin,
    isAgent
  } = getUser()

  viewRequestTableAgent({
    rows: filteredRequests,
    firstName,
    isLoggedIn,
    isAdmin,
    isAgent
  })
}

const handleClickPrintPDF = () => {
  generatePDF()
}

const handleClickSendPDFEmail = async () => {
  await sendPDFEmail()
  $('#policyPDFEmail').hide()
  $('#emailSuccess').removeAttr('hidden')
}

const handleClickShow = async () => {
  const token = getToken()
  const currData = await showTable(token)
  let newData
  const masterArray = []
  currData.forEach((data) => {
    const ccMin = data.cubicCapacityMin
    const ccMax = data.cubicCapacityMax
    const carAgeMin = data.carAgeMin
    const carAgeMax = data.carAgeMax
    const baseAmount = data.baseAmount
    const tableState = data.tableState
    const dateOfActivation = data.dateOfActivation
    newData = [ccMin, ccMax, carAgeMin, carAgeMax, baseAmount, tableState, dateOfActivation]
    masterArray.push(newData)
  })

  $('#showTable').empty()
  $('#showTable').jexcel({
    data: masterArray,
    colHeaders: ['cc_min', 'cc_max', 'carAge_min', 'carAge_max', 'base_amount', 'tableState', 'dateOfActivation'],
    colWidths: [140, 140, 140, 140, 150, 120, 140],
    columns: [{
        type: 'text'
      },
      {
        type: 'text'
      },
      {
        type: 'text'
      },
      {
        type: 'text'
      },
      {
        type: 'text'
      },
      {
        type: 'text'
      },
      {
        type: 'text'
      },
      {
        type: 'text'
      }
    ]
  })
}

const handleClickShowInsTaxes = async () => {
  const token = getToken()
  const currData = await showInsTaxes(token)
  let newData
  const masterArray = []
  currData.forEach((data) => {
    const date = data.dateOfActivation
    const total = data.taxAmount
    const state = data.tableState
    newData = [total, state, date]
    masterArray.push(newData)
  })

  $('#showTable').empty()
  $('#showTable').jexcel({
    data: masterArray,
    colHeaders: ['tax_amount', 'tableState', 'uploadDate'],
    colWidths: [140, 140, 140],
    columns: [{
        type: 'text'
      },
      {
        type: 'text'
      },
      {
        type: 'text'
      }
    ]
  })
}

const handleClickShowInsCoeff = async () => {
  const token = getToken()
  const currData = await showInsCoeff(token)
  let newData
  const masterArray = []
  currData.forEach((data) => {
    const acc = data.accident
    const underAge = data.ageUnder25
    const date = data.dateOfActivation
    const state = data.tableState
    const totalCoeff = data.totalCoefficient
    newData = [acc, underAge, totalCoeff, state, date]
    masterArray.push(newData)
  })

  $('#showTable').empty()
  $('#showTable').jexcel({
    data: masterArray,
    colHeaders: ['has_accidents', 'age_under_25', 'total_coefficient', 'table_state', 'upload_date'],
    colWidths: [150, 150, 150, 150, 150],
    columns: [{
        type: 'text'
      },
      {
        type: 'text'
      },
      {
        type: 'text'
      },
      {
        type: 'text'
      },
      {
        type: 'text'
      }
    ]
  })
}

const handleClickNextPage = (event) => {
  $('#left-page').text(+$('#left-page').text() + 1)
  $('#mid-page').text(+$('#mid-page').text() + 1)
  $('#right-page').text(+$('#right-page').text() + 1)
}

const handleClickPreviousPage = (event) => {

  if (+$('#left-page').text() > 1) {
    $('#left-page').text(+$('#left-page').text() - 1)
    $('#mid-page').text(+$('#mid-page').text() - 1)
    $('#right-page').text(+$('#right-page').text() - 1)
  }
}

const handleClickPage = async (event) => {
  const pageNumber = $(event.target).text()
  const token = sessionStorage.getItem('authToken')
  const sessionRequest = sessionStorage.getItem('requests')
  if (sessionRequest === 'pending') {
    let pendingRequests = await getAllPendingRequests(token, pageNumber - 1)
    pendingRequests = pendingRequests.content.map(request => {
      request.isPending = request.policyState === 'PENDING'
      return request
    })
    const {
      isLoggedIn,
      firstName,
      isAdmin,
      isAgent
    } = getUser()

    viewRequestTableAgent({
      rows: pendingRequests,
      firstName,
      isLoggedIn,
      isAdmin,
      isAgent,
      filter: true
    })
  } else if (sessionRequest === 'approved') {
    let approvedRequests = await getAllApprovedRequests(token, pageNumber - 1)
    approvedRequests = approvedRequests.content.map(request => {
      request.isApproved = request.policyState === 'APPROVED'
      return request
    })
    const {
      isLoggedIn,
      firstName,
      isAdmin,
      isAgent
    } = getUser()

    viewRequestTableAgent({
      rows: approvedRequests,
      firstName,
      isLoggedIn,
      isAdmin,
      isAgent,
      filter: true
    })
  } else if (sessionRequest === 'declined') {
    let declinedRequests = await getAllDeclinedRequests(token, pageNumber - 1)
    declinedRequests = declinedRequests.content.map(request => {
      request.isDeclined = request.policyState === 'DECLINED'
      return request
    })

    const {
      isLoggedIn,
      firstName,
      isAdmin,
      isAgent
    } = getUser()

    viewRequestTableAgent({
      rows: declinedRequests,
      firstName,
      isLoggedIn,
      isAdmin,
      isAgent,
      filter: true
    })
  } else {
    let allRequests = await getAllRequests(token, pageNumber - 1)
    allRequests = allRequests.content.map(request => {
      request.isPending = request.policyState === 'PENDING'
      request.isApproved = request.policyState === 'APPROVED'
      request.isDeclined = request.policyState === 'DECLINED'
      return request
    })

    const {
      isLoggedIn,
      firstName,
      isAdmin,
      isAgent
    } = getUser()

    viewRequestTableAgent({
      rows: allRequests,
      firstName,
      isLoggedIn,
      isAdmin,
      isAgent,
      filter: true
    })
  }
}

export {
  handleOnLoadHome,
  handleClickHome,
  handleClickAbout,
  handleClickRegister,
  handleClickLogin,
  handleClickLoginButton,
  handleClickOffer,
  handleClickGetStarted,
  handleClickMakeOption,
  handleClickModelOption,
  handleClickDriverInfo,
  handleClickMyRequests,
  handleClickRequestCardInfo,
  handleClickContact,
  handleClickMyProfile,
  handleClickRequestTable,
  handleClickGetPrice,
  handleClickOfferBack,
  handleClickDriverBack,
  handleClickODBack,
  handleClickNo,
  handleClickYes,
  handleClickLogout,
  handleClickAllRequests,
  handleClickAllPendingRequests,
  handleClickAllApprovedRequests,
  handleClickAllDeclinedRequests,
  handleClickFilterPage,
  handleClickClearAllFilters,
  handleClickSearchByPhoneNumber,
  handleClickSearchByEmail,
  handleClickApproveRequest,
  handleClickDeclineRequest,
  handleClickAllUsers,
  handleClickOnPolicyModalLogin,
  handleClickRequestPolicy,
  handleClickOnRegDate,
  handleClickCancelRequest,
  handleClickSavePolicy,
  handleClickEditUser,
  handleClickCreateAccount,
  handleClickUploadTable,
  handleClickUpload,
  handleClickFilterByRequestDate,
  handleClickPrintPDF,
  handleClickSendPDFEmail,
  handleClickShow,
  handleClickShowInsTaxes,
  handleClickShowInsCoeff,
  handleClickNextPage,
  handleClickPreviousPage,
  handleClickPage
}