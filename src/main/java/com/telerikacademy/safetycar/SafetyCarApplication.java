package com.telerikacademy.safetycar;


import com.telerikacademy.safetycar.configuration.FileStorageProperties;
import com.telerikacademy.safetycar.security.AuthenticationService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class SafetyCarApplication  {

 //  public class SafetyCarApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SafetyCarApplication.class, args);
    }


    @Autowired
    AuthenticationService authenticationService;


    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

//    @Override
//    public void run(String... params) {
//
//        User admin = new User();
//        admin.setFirstName("admin");
//        admin.setLastName("admin");
//        admin.setPassword("admin");
//        admin.setEmail("admin@email.com");
//        admin.setRole(Role.ROLE_ADMIN);
//
//        authenticationService.signup(admin);
//
//        User agent = new User();
//        agent.setFirstName("agent");
//        agent.setLastName("agent");
//        agent.setPassword("agent");
//        agent.setEmail("agent@email.com");
//        agent.setRole(Role.ROLE_AGENT);
//
//        authenticationService.signup(agent);
//
//        User client = new User();
//        client.setFirstName("client");
//        client.setLastName("client");
//        client.setPassword("client");
//        client.setEmail("client@email.com");
//        client.setRole(Role.ROLE_CLIENT);
//
//        authenticationService.signup(client);
//    }
}
