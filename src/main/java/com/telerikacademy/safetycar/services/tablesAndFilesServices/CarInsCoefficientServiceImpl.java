package com.telerikacademy.safetycar.services.tablesAndFilesServices;

import com.telerikacademy.safetycar.models.CoefficientTables.CarInsCoefficient;
import com.telerikacademy.safetycar.models.CoefficientTables.CarInsCoefficientTemp;
import com.telerikacademy.safetycar.models.Enums.TableState;
import com.telerikacademy.safetycar.repositories.CarInsCoefficientRepository;
import com.telerikacademy.safetycar.repositories.CarInsCoefficientTempRepository;
import com.telerikacademy.safetycar.services.serviceContracts.CarInsCofficientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class CarInsCoefficientServiceImpl implements CarInsCofficientService {

    private final CarInsCoefficientTempRepository carInsCoefficentTempRepository;
    private final CarInsCoefficientRepository carInsCoefficentRepository;

    @Autowired
    public CarInsCoefficientServiceImpl(CarInsCoefficientRepository carInsCoefficentRepository,
                                        CarInsCoefficientTempRepository carInsCoefficentTempRepository) {
        this.carInsCoefficentTempRepository = carInsCoefficentTempRepository;
        this.carInsCoefficentRepository = carInsCoefficentRepository;
    }

    public CarInsCoefficientTemp saveTempTable(ArrayList<String> inputLine) {
        try {
            CarInsCoefficientTemp tempRow = new CarInsCoefficientTemp();

            tempRow.setId(Integer.parseInt(inputLine.get(0)));
            tempRow.setAccident(Integer.parseInt(inputLine.get(1)));
            tempRow.setAgeUnder25(Integer.parseInt(inputLine.get(2)));
            tempRow.setTotalCoefficient(Double.parseDouble(inputLine.get(3)));
            tempRow.setTableState(TableState.ACTIVE);
            tempRow.setDateOfActivation(LocalDate.now());

         return carInsCoefficentTempRepository.save(tempRow);

        } catch (Exception ex) {
            carInsCoefficentTempRepository.deleteAll();
            throw new IllegalArgumentException("Data in table is not correct");
        }

    }


    public void setAllRowsAsInactive() {

        if (carInsCoefficentRepository.findAll() != null) {
            carInsCoefficentRepository.setInactiveOldTable();
        }
    }

    public void saveTempTableToOriginal() {

        carInsCoefficentRepository.saveTempTableToOriginal();
    }

    public void deleteTempTable() {

        carInsCoefficentTempRepository.deleteAll();
    }
    public List<CarInsCoefficient> findAllByTableStateActive() {
        return carInsCoefficentRepository.findAllByTableStateActive();
    }
}
