package com.telerikacademy.safetycar.services.tablesAndFilesServices;

import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmount;
import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmountTemp;
import com.telerikacademy.safetycar.models.Enums.TableState;
import com.telerikacademy.safetycar.repositories.CarBaseAmountRepository;
import com.telerikacademy.safetycar.repositories.CarBaseAmountTempRepository;
import com.telerikacademy.safetycar.services.serviceContracts.CarBaseAmountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class CarBaseAmountServiceImpl implements CarBaseAmountService {

    private final CarBaseAmountRepository carBaseAmountRepository;
    private final CarBaseAmountTempRepository carBaseAmountTempRepository;


    @Autowired
    public CarBaseAmountServiceImpl(CarBaseAmountRepository carBaseAmountRepository, CarBaseAmountTempRepository carBaseAmountTempRepository) {
        this.carBaseAmountRepository = carBaseAmountRepository;
        this.carBaseAmountTempRepository = carBaseAmountTempRepository;
    }

    public void setAllRowsAsInactive() {

        if (carBaseAmountRepository.findAll() != null) {
            carBaseAmountRepository.setInactiveOldTable();
        }
           }

    public void saveTempTableToOriginal() {

        carBaseAmountRepository.saveTempTableToOriginal();
    }

    public void deleteTempTable() {

        carBaseAmountTempRepository.deleteAll();
    }

    public CarBaseAmountTemp saveTempTable(ArrayList<String> inputLine) {

        try {
            CarBaseAmountTemp tempRow = new CarBaseAmountTemp();

            tempRow.setId(Integer.parseInt(inputLine.get(0)));
            tempRow.setCubicCapacityMin(Integer.parseInt(inputLine.get(1)));
            tempRow.setCubicCapacityMax(Integer.parseInt(inputLine.get(2)));
            tempRow.setCarAgeMin(Integer.parseInt(inputLine.get(3)));
            tempRow.setCarAgeMax(Integer.parseInt(inputLine.get(4)));
            tempRow.setBaseAmount(Double.parseDouble(inputLine.get(5)));
            tempRow.setTableState(TableState.ACTIVE);
            tempRow.setDateOfActivation(LocalDate.now());

            return carBaseAmountTempRepository.save(tempRow);

        } catch (Exception ex) {

            carBaseAmountTempRepository.deleteAll();

            throw new IllegalArgumentException("Data in table is not correct");
        }

    }

    public List<CarBaseAmount> findAllByTableStateActive() {
        return carBaseAmountRepository.findAllByTableStateActive();
    }


}
