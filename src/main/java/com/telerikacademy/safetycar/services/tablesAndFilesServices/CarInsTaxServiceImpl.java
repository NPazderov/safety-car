package com.telerikacademy.safetycar.services.tablesAndFilesServices;

import com.telerikacademy.safetycar.models.CoefficientTables.CarInsTaxes;
import com.telerikacademy.safetycar.models.CoefficientTables.CarInsTaxesTemp;
import com.telerikacademy.safetycar.models.Enums.TableState;
import com.telerikacademy.safetycar.repositories.CarInsTaxesRepository;
import com.telerikacademy.safetycar.repositories.CarInsTaxesTempRepository;
import com.telerikacademy.safetycar.services.serviceContracts.CarInsTaxesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class CarInsTaxServiceImpl implements CarInsTaxesService {

    private final CarInsTaxesRepository carInsTaxesRepository;
    private final CarInsTaxesTempRepository carInsTaxesTempRepository;

    @Autowired
    public CarInsTaxServiceImpl(CarInsTaxesRepository carInsTaxesRepository, CarInsTaxesTempRepository carInsTaxesTempRepository) {
        this.carInsTaxesRepository = carInsTaxesRepository;
        this.carInsTaxesTempRepository = carInsTaxesTempRepository;
    }


    public CarInsTaxesTemp saveTempTable(ArrayList<String> inputLine) {

        try {
            CarInsTaxesTemp tempRow = new CarInsTaxesTemp();

            tempRow.setId(Integer.parseInt(inputLine.get(0)));
            tempRow.setTaxAmount(Double.parseDouble(inputLine.get(1)));
            tempRow.setTableState(TableState.ACTIVE);
            tempRow.setDateOfActivation(LocalDate.now());

             return carInsTaxesTempRepository.save(tempRow);

        } catch (Exception ex) {
            carInsTaxesTempRepository.deleteAll();
            throw new IllegalArgumentException("Data in table is not correct");
        }
    }


    public void setAllRowsAsInactive() {

        if (carInsTaxesRepository.findAll() != null) {
            carInsTaxesRepository.setInactiveOldTable();
        }
    }

    public void saveTempTableToOriginal() {

        carInsTaxesRepository.saveTempTableToOriginal();
    }

    public void deleteTempTable() {

        carInsTaxesTempRepository.deleteAll();
    }

    public List<CarInsTaxes> findAllByTableStateActive() {

        return carInsTaxesRepository.findAllByTableStateActive();
    }

}
