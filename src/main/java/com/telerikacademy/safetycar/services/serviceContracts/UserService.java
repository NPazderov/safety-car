package com.telerikacademy.safetycar.services.serviceContracts;

import com.telerikacademy.safetycar.models.CarInsPolicy;
import com.telerikacademy.safetycar.models.DTOs.UserRequestDTO;
import com.telerikacademy.safetycar.models.User;


import java.util.List;

public interface UserService {

    List<User> findAll();

   User findByEmail(String email);

    User findById(Long id);

   User  update(Long id, UserRequestDTO newUser);

    User getCurrentUser();

    List<CarInsPolicy> getUserRequests(String email);

}
