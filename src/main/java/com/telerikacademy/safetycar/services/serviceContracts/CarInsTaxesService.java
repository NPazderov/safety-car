package com.telerikacademy.safetycar.services.serviceContracts;

import com.telerikacademy.safetycar.models.CoefficientTables.CarInsTaxes;
import com.telerikacademy.safetycar.models.CoefficientTables.CarInsTaxesTemp;

import java.util.ArrayList;
import java.util.List;

public interface CarInsTaxesService {

    void saveTempTableToOriginal();

    void deleteTempTable();

    void setAllRowsAsInactive();

    CarInsTaxesTemp saveTempTable(ArrayList<String> inputLine);

    List<CarInsTaxes> findAllByTableStateActive();
}
