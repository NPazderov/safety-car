package com.telerikacademy.safetycar.services.serviceContracts;

import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;

import java.time.LocalDate;


public interface GetQuoteService {


    double calculateTotalPremium(CarInsPolicyDTO carInsPolicyDTO);

    int calculateAge(LocalDate dateInPast);

    boolean isUnder25(LocalDate dateOfBirth);

    boolean policyUserAgeIsCorrect(LocalDate dateOfBirth);

    boolean carAgeIsCorrect(LocalDate registrationDate);

}
