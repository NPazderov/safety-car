package com.telerikacademy.safetycar.services.serviceContracts;

import com.telerikacademy.safetycar.models.Car;

import java.util.List;

public interface CarService {
    List<Car> findAll();
    List<Car> findAllModelsByGivenMake(String make);
    List<Car> findAllModelsByGivenModel(String model);
}
