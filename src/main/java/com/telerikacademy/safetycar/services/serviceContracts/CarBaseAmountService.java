package com.telerikacademy.safetycar.services.serviceContracts;

import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmount;
import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmountTemp;

import java.util.ArrayList;
import java.util.List;

public interface CarBaseAmountService {

    void saveTempTableToOriginal();
    void setAllRowsAsInactive();
    void deleteTempTable();

    CarBaseAmountTemp saveTempTable(ArrayList<String> inputLine);

    List<CarBaseAmount> findAllByTableStateActive();

}
