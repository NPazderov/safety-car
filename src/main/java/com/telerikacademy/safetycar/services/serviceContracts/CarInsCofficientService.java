package com.telerikacademy.safetycar.services.serviceContracts;

import com.telerikacademy.safetycar.models.CoefficientTables.CarInsCoefficient;
import com.telerikacademy.safetycar.models.CoefficientTables.CarInsCoefficientTemp;

import java.util.ArrayList;
import java.util.List;

public interface CarInsCofficientService {


    void saveTempTableToOriginal();

    void deleteTempTable();

    void setAllRowsAsInactive();

    CarInsCoefficientTemp saveTempTable(ArrayList<String> inputLine);

    List<CarInsCoefficient> findAllByTableStateActive();
}
