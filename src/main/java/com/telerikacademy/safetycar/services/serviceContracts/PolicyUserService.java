package com.telerikacademy.safetycar.services.serviceContracts;


import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.models.PolicyUser;



public interface PolicyUserService {

     PolicyUser save(CarInsPolicyDTO carInsPolicyDTO);
}
