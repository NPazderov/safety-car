package com.telerikacademy.safetycar.services.serviceContracts;

import com.telerikacademy.safetycar.models.ContactInfo;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;

public interface ContactInfoService {

     ContactInfo save(CarInsPolicyDTO carInsPolicyDTO);
}
