package com.telerikacademy.safetycar.services.serviceContracts;

import com.telerikacademy.safetycar.models.CarInsPolicy;
import com.telerikacademy.safetycar.models.ContactInfo;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.models.PolicyUser;
import com.telerikacademy.safetycar.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CarInsPolicyService {

    Optional<CarInsPolicy> findById(long id);

       Page<CarInsPolicy> findAll(int page);
//    List<CarInsPolicy> findAll();

    List<CarInsPolicy> findAllByUserEmail(String email);

    Page<CarInsPolicy> findAllByPolicyState(String status, int page);

    CarInsPolicy save(CarInsPolicyDTO carInsPolicyDTO, String imageReference,
                      User loggedUSer, ContactInfo contactInfo, PolicyUser user, int driverAge);

    boolean checkStartPolicyDate(LocalDate startPolicyDate);

    Optional<CarInsPolicy> approveRequestById(long requestId) throws Exception;

    Optional<CarInsPolicy> declineRequestById(long requestId) throws Exception;

    Optional<CarInsPolicy> cancelRequestById(long requestId);

    List<CarInsPolicy> findAllByPhoneNumber(String phoneNumber);

    List<CarInsPolicy> findAllByPolicyUserEmail(String email);

    List<CarInsPolicy> findAllByRequestDate(LocalDate fromDate, LocalDate toDate);

}
