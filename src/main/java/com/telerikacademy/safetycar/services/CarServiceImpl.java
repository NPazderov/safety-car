package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.repositories.CarRepository;
import com.telerikacademy.safetycar.services.serviceContracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public List<Car> findAll() {
        return carRepository.findAll();
    }

    @Override
    public List<Car> findAllModelsByGivenMake(String make) {
        return carRepository.findAllModelsByGivenMake(make);
    }

    @Override
    public List<Car> findAllModelsByGivenModel(String model) {
        return carRepository.findAllModelsByGivenModel(model);
    }
}
