package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.ContactInfo;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.models.PolicyUser;
import com.telerikacademy.safetycar.repositories.ContactInfoRepository;
import com.telerikacademy.safetycar.repositories.PolicyUserRepository;
import com.telerikacademy.safetycar.services.serviceContracts.ContactInfoService;
import com.telerikacademy.safetycar.services.serviceContracts.PolicyUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PolicyUserServiceImpl implements PolicyUserService {

    private final PolicyUserRepository policyUserRepository;

    @Autowired
    public PolicyUserServiceImpl(PolicyUserRepository policyUserRepository) {
        this.policyUserRepository = policyUserRepository;
    }


    public PolicyUser save(CarInsPolicyDTO carInsPolicyDTO) {

        PolicyUser policyUser = new PolicyUser();

        policyUser.setFirstName(carInsPolicyDTO.getFirstName());
        policyUser.setLastName(carInsPolicyDTO.getLastName());

        policyUserRepository.save(policyUser);


        return policyUser;
    }


}
