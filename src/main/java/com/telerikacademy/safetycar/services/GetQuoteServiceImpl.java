package com.telerikacademy.safetycar.services;


import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.repositories.CarBaseAmountRepository;
import com.telerikacademy.safetycar.repositories.CarInsCoefficientRepository;
import com.telerikacademy.safetycar.repositories.CarInsTaxesRepository;
import com.telerikacademy.safetycar.services.serviceContracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;

@Service
public class GetQuoteServiceImpl implements GetQuoteService {


    private final CarBaseAmountRepository carBaseAmountRepository;
    private final CarInsCoefficientRepository carInsCoefficentRepository;
    private final CarInsTaxesRepository carInsTaxesRepository;

    @Autowired
    public GetQuoteServiceImpl(CarBaseAmountRepository carBaseAmountRepository, CarInsCoefficientRepository carInsCoefficentRepository,
                               CarInsTaxesRepository carInsTaxesRepository) {
        this.carBaseAmountRepository = carBaseAmountRepository;
        this.carInsCoefficentRepository = carInsCoefficentRepository;
        this.carInsTaxesRepository = carInsTaxesRepository;
    }

    @Override
    public double calculateTotalPremium(CarInsPolicyDTO carInsPolicyDTO) {

        if (!carAgeIsCorrect(carInsPolicyDTO.getFirstRegistrationDate())) {
            throw new IllegalArgumentException(
                    " Registration date can not be in future!");
        }

        if (!policyUserAgeIsCorrect(carInsPolicyDTO.getDateOfBirth())) {
            throw new IllegalArgumentException(
                    "Our insurance company will provide a quote once you're 18 years old");
        }

        int carAge = calculateAge(carInsPolicyDTO.getFirstRegistrationDate());
        int cubicCapacity = carInsPolicyDTO.getCar().getCubicCapacity();
        double carBaseAmount = carBaseAmountRepository.findByCarAgeAndCubicCapacity(carAge, cubicCapacity);
        boolean underAge = isUnder25(carInsPolicyDTO.getDateOfBirth());
        boolean hasAccident = carInsPolicyDTO.isAccidents();
        double totalCoeff = carInsCoefficentRepository.findByAgeUnder25AndAccident(underAge, hasAccident);
        double netPremium = carBaseAmount * totalCoeff;
        double totalTaxes = carInsTaxesRepository.findTax() * netPremium;

        return netPremium + totalTaxes;
    }

    public boolean isUnder25(LocalDate dateOfBirth) {

        int age = calculateAge(dateOfBirth);
        return age < 25;
    }

    public boolean policyUserAgeIsCorrect(LocalDate dateOfBirth) {

        int age = calculateAge(dateOfBirth);
        return age >= 18;
    }

    public boolean carAgeIsCorrect(LocalDate registrationDate) {

        int carAge = calculateAge(registrationDate);
        return carAge >= 0;
    }

    public int calculateAge(LocalDate dateInPast) {

        LocalDate now = LocalDate.now();
        Period diff = Period.between(dateInPast, now);

    return diff.getYears();
    }

}
