package com.telerikacademy.safetycar.services;


import com.telerikacademy.safetycar.exceptions.CustomException;
import com.telerikacademy.safetycar.exceptions.EmailExistsException;
import com.telerikacademy.safetycar.models.CarInsPolicy;
import com.telerikacademy.safetycar.models.DTOs.UserRequestDTO;
import com.telerikacademy.safetycar.models.Enums.Role;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.VerificationToken;
import com.telerikacademy.safetycar.repositories.UserRepository;
import com.telerikacademy.safetycar.repositories.VerificationTokenRepository;
import com.telerikacademy.safetycar.services.serviceContracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;


@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;

    }


    public User findByEmail(String email) {
        User user = userRepository.findByEmail(email);
        if (user ==null) {
            throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
        }
        return user;
    }


    public List<User> findAll() {

        return userRepository.findAll();
    }


    @Override
    public User findById(Long id) {

        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent()) {
            throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
        }
        return user.get();
    }

    @Override
    public User update(Long id, UserRequestDTO newUser) {

        User user = findById(id);

        user.setPassword(passwordEncoder.encode(newUser.getPassword()));
        user.setEmail(newUser.getEmail());
        user.setFirstName(newUser.getFirstName());
        user.setLastName(newUser.getLastName());

        userRepository.save(user);

        return user;
    }

    @Override
    public List<CarInsPolicy> getUserRequests(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("User with email %s doesn't exist ", email));
        }
        return user.getUserCarInsPolicy();
    }

    @Override
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().
                getAuthentication();
        return findByEmail(authentication.getName());
    }

}