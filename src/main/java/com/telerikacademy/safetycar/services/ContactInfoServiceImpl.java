package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.ContactInfo;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.repositories.ContactInfoRepository;
import com.telerikacademy.safetycar.services.serviceContracts.ContactInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ContactInfoServiceImpl implements ContactInfoService {

    private final ContactInfoRepository contactInfoRepository;

    @Autowired
    public ContactInfoServiceImpl(ContactInfoRepository contactInfoRepository) {
        this.contactInfoRepository = contactInfoRepository;
    }

    @Override
    public ContactInfo save(CarInsPolicyDTO carInsPolicyDTO) {

        ContactInfo contactInfo = new ContactInfo();

        contactInfo.setPhoneNumber(carInsPolicyDTO.getPhone());
        contactInfo.setTown(carInsPolicyDTO.getTown());
        contactInfo.setAddress(carInsPolicyDTO.getAddress());
        contactInfo.setPostalCode(carInsPolicyDTO.getPostalCode());
        contactInfo.setPolicyEmail(carInsPolicyDTO.getPolicyEmail());

        contactInfoRepository.save(contactInfo);

        return contactInfo;
    }


}
