package com.telerikacademy.safetycar.services;


import com.telerikacademy.safetycar.models.CarInsPolicy;
import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmount;
import com.telerikacademy.safetycar.models.ContactInfo;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.models.Enums.PolicyState;
import com.telerikacademy.safetycar.models.PolicyUser;
import com.telerikacademy.safetycar.models.User;

import com.telerikacademy.safetycar.repositories.*;
import com.telerikacademy.safetycar.services.serviceContracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class CarInsPolicyServiceImpl implements CarInsPolicyService {


    private CarInsPolicyRepository carInsPolicyRepository;
    private UserRepository userRepository;
    private CarBaseAmountRepository carBaseAmountRepository;

    @Autowired
    public CarInsPolicyServiceImpl(CarInsPolicyRepository carInsPolicyRepository,
                                   UserRepository userRepository,
                                   CarBaseAmountRepository carBaseAmountRepository) {
        this.carInsPolicyRepository = carInsPolicyRepository;
        this.userRepository = userRepository;
        this.carBaseAmountRepository = carBaseAmountRepository;
    }

    @Override
    public Optional<CarInsPolicy> findById(long requestId) {
        return carInsPolicyRepository.findById(requestId);
    }


    @Override
    public List<CarInsPolicy> findAllByUserEmail(String email) {
        return carInsPolicyRepository.findAllByUserEmail(email);
    }

    @Override
    public Page<CarInsPolicy> findAllByPolicyState(String status, int page) {
        PageRequest pageRequest = pageRequest(page);
        return carInsPolicyRepository.findAllByPolicyState(status, pageRequest);
    }

    @Override
    public CarInsPolicy save(CarInsPolicyDTO carInsPolicyDTO, String imageReference, User loggedUser, ContactInfo contact,
                             PolicyUser policyUser, int driverAge) {

        CarInsPolicy carInsPolicy = new CarInsPolicy();

        if (checkStartPolicyDate(carInsPolicyDTO.getStartPolicyDate())) {

            carInsPolicy.setUser(loggedUser);
            carInsPolicy.setContact(contact);
            carInsPolicy.setPolicyState(PolicyState.PENDING);
            carInsPolicy.setPremium(carInsPolicyDTO.getPremium());
            carInsPolicy.setFirstRegistrationDate(carInsPolicyDTO.getFirstRegistrationDate());
            carInsPolicy.setQuoteDate(LocalDateTime.now());
            carInsPolicy.setCarCertificateImage(imageReference);
            carInsPolicy.setAccidents(carInsPolicyDTO.isAccidents());
            carInsPolicy.setStartPolicyDate(carInsPolicyDTO.getStartPolicyDate());
            carInsPolicy.setDriverAge(driverAge);
            carInsPolicy.setCar(carInsPolicyDTO.getCar());
            carInsPolicy.setPolicyUser(policyUser);
            carInsPolicy.setRateDate(carBaseAmountRepository.findActiveRateDate());

        }
        return carInsPolicyRepository.save(carInsPolicy);
    }


    public boolean checkStartPolicyDate(LocalDate startPolicyDate) {

        LocalDate now = LocalDate.now();
        long noOfDaysBetween = ChronoUnit.DAYS.between(now, startPolicyDate);

        if (noOfDaysBetween >= 0 && noOfDaysBetween <= 7) {
            return true;
        }
        if (noOfDaysBetween > 7) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Policy can't start after more than a week!");
        }

        throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Start policy date can't be in past!");
    }


    @Override
    public Optional<CarInsPolicy> approveRequestById(long requestId) throws Exception {

        Optional<CarInsPolicy> currentPolicy = carInsPolicyRepository.findById(requestId);
        if (!currentPolicy.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("Request with id %d doesn't exist ", requestId));
        }
        currentPolicy.get().setPolicyState(PolicyState.APPROVED);
        carInsPolicyRepository.save(currentPolicy.get());

        return currentPolicy;
    }

    @Override
    public Optional<CarInsPolicy> declineRequestById(long requestId) throws Exception {

        Optional<CarInsPolicy> currentPolicy = carInsPolicyRepository.findById(requestId);

        if (!currentPolicy.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("Request with id %d doesn't exist ", requestId));
        }
        currentPolicy.get().setPolicyState(PolicyState.DECLINED);
        carInsPolicyRepository.save(currentPolicy.get());

        return currentPolicy;
    }


    public Optional<CarInsPolicy> cancelRequestById(long requestId) {

        Optional<CarInsPolicy> currentPolicy = findById(requestId);

        if (!currentPolicy.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("Request with id %d doesn't exist ", requestId));
        }
        currentPolicy.get().setPolicyState(PolicyState.UNACTIVE);

        Optional<User> admin = userRepository.findById(1L);
        if (admin.isPresent()) {
            currentPolicy.get().setUser(admin.get());
        } else {
            throw new IllegalArgumentException("Can't set policy to admin! Admin not found");
        }

        carInsPolicyRepository.save(currentPolicy.get());

        return currentPolicy;
    }

    @Override
    public List<CarInsPolicy> findAllByPhoneNumber(String phoneNumber) {
        return carInsPolicyRepository.findAllByPhoneNumber(phoneNumber);
    }

    @Override
    public List<CarInsPolicy> findAllByPolicyUserEmail(String email) {
        return carInsPolicyRepository.findAllByPolicyUserEmail(email);
    }

    public List<CarInsPolicy> findAllByRequestDate(LocalDate fromDate, LocalDate toDate) {
        return carInsPolicyRepository.findAllByRequestDate(fromDate, toDate);
    }


    private PageRequest pageRequest(int page) {
        if (page < 0) {
            page = 0;
        }
        return PageRequest.of(page, 5);
    }

    public Page<CarInsPolicy> findAll(int page) {

        PageRequest pageRequest = pageRequest(page);
        return carInsPolicyRepository.findAll(pageRequest);
    }


}
