//package com.telerikacademy.safetycar.controllers;
//
//import com.telerikacademy.safetycar.models.Image;
//import com.telerikacademy.safetycar.utils.fileStorage.ImageService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.*;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//@CrossOrigin
//@RestController
//@RequestMapping("/api/images")
//public class ImageController {
//    private final ImageService imageService;
//
//    @Autowired
//    public ImageController(ImageService imageService) {
//        this.imageService = imageService;
//    }
////
//////    @PreAuthorize("hasRole('ROLE_CLIENT')or hasRole('ROLE_AGENT') or hasRole('ROLE_ADMIN')")
////    @PostMapping
////    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') or hasRole('ROLE_CLIENT') ")
////    public ResponseEntity<Image> save(@RequestParam(name = "image") final MultipartFile imageData) {
////        return imageService.save(imageData)
////                .map(record -> ResponseEntity.ok().body(record))
////                .orElse(ResponseEntity.badRequest().build());
////    }
////
////
////    @GetMapping("/{id}")
////    @PreAuthorize("permitAll()")
////    public ResponseEntity<byte[]> get(@PathVariable final int id) {
////        HttpHeaders headers = new HttpHeaders();
////        headers.setContentType(MediaType.IMAGE_JPEG);
////        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
////
////        return imageService.findById(id)
////                .map(value -> new ResponseEntity<>(value.getBytes(), headers, HttpStatus.OK))
////                .orElseGet(() -> ResponseEntity.notFound().build());
////
////    }
//
//
//    @GetMapping("/downloadFile/{fileName:.+}")
//    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') or hasRole('ROLE_CLIENT') ")
//    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
//        // Load file as Resource
//        Resource resource = fileStorageService.loadFileAsResource(fileName);
//
//        // Try to determine file's content type
//        String contentType = null;
//        try {
//            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
//        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
//        }
//
//        // Fallback to the default content type if type could not be determined
//        if(contentType == null) {
//            contentType = "application/octet-stream";
//        }
//
//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(contentType))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
//                .body(resource);
//    }
//}
//
//
//
//
//}
//

package com.telerikacademy.safetycar.controllers;

import com.telerikacademy.safetycar.models.DTOs.UploadFileResponseDTO;
import com.telerikacademy.safetycar.utils.fileStorage.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping("/api/images")
public class ImageController {

    private static final Logger logger = LoggerFactory.getLogger(UploadFileController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @CrossOrigin
    @PostMapping("/uploadImage")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') or hasRole('ROLE_CLIENT') ")
    public UploadFileResponseDTO uploadFile( MultipartFile file) {
      //  public UploadFileResponseDTO uploadFile(@RequestParam("image") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/images/downloadFile/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponseDTO(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @CrossOrigin
    @GetMapping("/downloadFile/{fileName:.+}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') or hasRole('ROLE_CLIENT') ")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}


