package com.telerikacademy.safetycar.controllers;


import com.itextpdf.text.DocumentException;
import com.telerikacademy.safetycar.utils.email.EmailService;
import com.telerikacademy.safetycar.models.CarInsPolicy;
import com.telerikacademy.safetycar.models.DTOs.UserRequestDTO;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.utils.pdf.PdfService;
import com.telerikacademy.safetycar.services.serviceContracts.CarInsPolicyService;
import com.telerikacademy.safetycar.services.serviceContracts.UserService;
import com.telerikacademy.safetycar.utils.fileStorage.FileStorageService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/users")
public class UserController {

    private UserService userService;
    private ModelMapper modelMapper;
    private CarInsPolicyService carInsPolicyService;
    private com.telerikacademy.safetycar.utils.pdf.PdfService pdfService;
    private EmailService emailService;
    private FileStorageService fileStorageService;

    @Autowired
    public UserController(UserService userService, ModelMapper modelMapper, CarInsPolicyService carInsPolicyService, PdfService pdfService, EmailService emailService, FileStorageService fileStorageService) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.carInsPolicyService = carInsPolicyService;
        this.pdfService = pdfService;
        this.emailService = emailService;
        this.fileStorageService = fileStorageService;
    }
//
//    @CrossOrigin
//    @GetMapping("/{email}")
//    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT')")
//    public UserResponseDTO findByEmail(@PathVariable String email) {
//
//        try {
//            return modelMapper.map(userService.findByEmail(email), UserResponseDTO.class);
//        } catch (IllegalArgumentException ex) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
//        }
//    }

    @CrossOrigin
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/getAll")
    public List<User> findAll() {

        try {
            return userService.findAll();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @CrossOrigin
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') or hasRole('ROLE_CLIENT') ")
    public User update(@PathVariable long id, @RequestBody UserRequestDTO newUser) {

        try {
            return userService.update(id, newUser);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @CrossOrigin
    @GetMapping("/requests")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') or hasRole('ROLE_CLIENT') ")
    public List<CarInsPolicy> getUserRequest(@RequestParam(value = "email") String email) {

        try {
            return userService.getUserRequests(email);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @CrossOrigin
    @PutMapping("/myRequests/cancel/{userId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') or hasRole('ROLE_CLIENT') ")
    public void cancelRequestById(@PathVariable long userId, @RequestParam(value = "requestId") long requestId) {

        try {
            Optional<CarInsPolicy> canceledPolicy = carInsPolicyService.cancelRequestById(requestId);
            if (canceledPolicy.isPresent()) {
                userService.getCurrentUser().getUserCarInsPolicy().remove(canceledPolicy.get());
            } else {
                throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("Request with id %d doesn't exist ", requestId));
            }

        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @CrossOrigin
    @GetMapping("/myRequests/printPreviewPolicy/{userId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') or hasRole('ROLE_CLIENT') ")
    public ResponseEntity<Resource> printPreviewPolicyByIds(@PathVariable long userId, @RequestParam(value = "requestId") long requestId) throws FileNotFoundException, DocumentException {

        User loggedUser = userService.getCurrentUser();
        String fileName = pdfService.printPDF(loggedUser, requestId);
        Resource resource = fileStorageService.loadFileAsResource(fileName);
        String contentType = "application/pdf";

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @CrossOrigin
    @GetMapping("/sendPDFEmail")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') or hasRole('ROLE_CLIENT') ")
    public String sendPDFEmail(@RequestParam long policyId ) throws Exception {

        User loggedUser = userService.getCurrentUser();
        String fileName = pdfService.printPDF(loggedUser, policyId);
        String path = "E:/01_Java/Telerik/SAFETY_CAR/Pictures/" + fileName;
        Optional<CarInsPolicy> carInsPolicy = carInsPolicyService.findById(policyId);
        if (carInsPolicy.isPresent()) {
            emailService.sendMessageWithAttachment(carInsPolicy.get().getContact().getPolicyEmail(), path);
        }

        return "Success";
    }


}
