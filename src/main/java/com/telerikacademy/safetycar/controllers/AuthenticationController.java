package com.telerikacademy.safetycar.controllers;

import com.telerikacademy.safetycar.models.DTOs.AuthenticationUserDTO;
import com.telerikacademy.safetycar.models.DTOs.UserRequestDTO;
import com.telerikacademy.safetycar.models.DTOs.UserResponseDTO;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.security.AuthenticationService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("api/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ModelMapper modelMapper;

    @CrossOrigin
    @PostMapping("/signup")
    public String signup(@Valid @RequestBody UserRequestDTO user) throws Exception {
        return authenticationService.signup(modelMapper.map(user, User.class));
    }

    @CrossOrigin
    @PostMapping("/login")
    public String login(@Valid @RequestBody AuthenticationUserDTO user) {
        return authenticationService.login(user);
    }

    @CrossOrigin
    @PostMapping("/logout")
    public String logout(HttpServletRequest req) {
        return null;
    }

    @CrossOrigin
    @GetMapping(value = "/me")
    public UserResponseDTO whoAmI(HttpServletRequest req) {
        return modelMapper.map(authenticationService.whoami(req), UserResponseDTO.class);
    }

    @CrossOrigin
    @RequestMapping(value="/confirm-account", method= {RequestMethod.GET, RequestMethod.POST})
    public String confirmUserAccount(@RequestParam("token")String verificationToken, HttpServletResponse response) throws IOException
    {
       return authenticationService.confirmUserAccount(verificationToken, response);
    }

}
