package com.telerikacademy.safetycar.controllers;


import com.telerikacademy.safetycar.utils.excelTablesReaderWriter.ExcelTableService;
import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmount;
import com.telerikacademy.safetycar.models.CoefficientTables.CarInsCoefficient;
import com.telerikacademy.safetycar.models.CoefficientTables.CarInsTaxes;
import com.telerikacademy.safetycar.services.serviceContracts.CarBaseAmountService;
import com.telerikacademy.safetycar.services.serviceContracts.CarInsCofficientService;
import com.telerikacademy.safetycar.services.serviceContracts.CarInsTaxesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/upload")
public class UploadFileController {

    private ExcelTableService excelTable;
    private CarBaseAmountService carBaseAmountService;
    private CarInsTaxesService carInsTaxesService;
    private CarInsCofficientService carInsCofficientService;

    @Autowired
    public UploadFileController(ExcelTableService excelTable, CarBaseAmountService carBaseAmountService, CarInsTaxesService carInsTaxesService, CarInsCofficientService carInsCofficientService) {
        this.excelTable = excelTable;
        this.carBaseAmountService = carBaseAmountService;
        this.carInsTaxesService = carInsTaxesService;
        this.carInsCofficientService = carInsCofficientService;
    }

    @CrossOrigin
    @PostMapping("/table")
   @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') ")
    public String uploadTable(@RequestParam("file") MultipartFile file) throws IOException {

        excelTable.readAndWriteToDatabase(file);

        return "Successful uploaded to database!";
    }

    @CrossOrigin
    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') ")
    public List<CarBaseAmount> findTableBaseAmount() {
       return carBaseAmountService.findAllByTableStateActive();
    }

    @CrossOrigin
    @GetMapping("/getTaxes")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') ")
    public List<CarInsTaxes> findTableTaxes() {
        return carInsTaxesService.findAllByTableStateActive();
    }

    @CrossOrigin
    @GetMapping("/getCoefficients")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT') ")
    public List<CarInsCoefficient> findTableCoefficient() {
        return carInsCofficientService.findAllByTableStateActive();
    }
}
