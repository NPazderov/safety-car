package com.telerikacademy.safetycar.controllers;

import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.services.serviceContracts.CarService;
import com.telerikacademy.safetycar.services.serviceContracts.GetQuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/getQuote")
public class GetQuoteController {

    private CarService carService;
    private GetQuoteService getQuoteService;

    @Autowired
    public GetQuoteController(CarService carService, GetQuoteService getQuoteService) {
        this.carService = carService;
        this.getQuoteService = getQuoteService;
    }

    @CrossOrigin
    @PostMapping("/result")
    public double getPremium(@Valid @RequestBody CarInsPolicyDTO offerDetails) {

        try {
            return getQuoteService.calculateTotalPremium(offerDetails);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @CrossOrigin
    @GetMapping("/carDetail")
    public List<Car> findAll() {

        try {
            return carService.findAll();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @CrossOrigin
    @GetMapping("/carModels")
    public List<Car> findAllByGivenMake(@RequestParam String make) {

        try {
            return carService.findAllModelsByGivenMake(make);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @CrossOrigin
    @GetMapping("/carCC")
    public List<Car> findAllByGivenModel(@RequestParam String model) {

        try {
            return carService.findAllModelsByGivenModel(model);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
}
