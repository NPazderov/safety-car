package com.telerikacademy.safetycar.controllers;

import com.telerikacademy.safetycar.models.CarInsPolicy;
import com.telerikacademy.safetycar.models.ContactInfo;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.models.DTOs.UploadFileResponseDTO;
import com.telerikacademy.safetycar.models.PolicyUser;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.services.serviceContracts.*;
import com.telerikacademy.safetycar.utils.fileStorage.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;


import javax.validation.Valid;

@RestController
@RequestMapping("api/applications/new")
public class ApplicantController {

    private CarInsPolicyService carInsPolicyService;
    private ImageService imageService;
    private UserService userService;
    private ContactInfoService contactInfoService;
    private PolicyUserService policyUserService;
    private GetQuoteService getQuoteService;

    @Autowired
    public ApplicantController(CarInsPolicyService carInsPolicyService, ImageService imageService, UserService userService,
                               ContactInfoService contactInfoService, PolicyUserService policyUserService, GetQuoteService getQuoteService) {
        this.carInsPolicyService = carInsPolicyService;
        this.imageService = imageService;
        this.userService = userService;
        this.contactInfoService = contactInfoService;
        this.policyUserService = policyUserService;
        this.getQuoteService = getQuoteService;
    }


    @CrossOrigin
    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT') or  hasRole('ROLE_AGENT')")
    public CarInsPolicy savePolicy(@Valid @ModelAttribute CarInsPolicyDTO offerDetails) {

        try {

            MultipartFile file = offerDetails.getCarCertificateImage();
            UploadFileResponseDTO response = imageService.saveImage(file);
            String imageReference = response.getFileDownloadUri();

            User loggedUser = userService.getCurrentUser();
            ContactInfo contactInfo = contactInfoService.save(offerDetails);

            PolicyUser policyUser = policyUserService.save(offerDetails);
            int driverAge = getQuoteService.calculateAge(offerDetails.getDateOfBirth());

            return carInsPolicyService.save(offerDetails, imageReference, loggedUser, contactInfo, policyUser, driverAge);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Test");
        }


    }

}