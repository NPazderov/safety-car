package com.telerikacademy.safetycar.controllers;

import com.telerikacademy.safetycar.utils.email.EmailService;
import com.telerikacademy.safetycar.models.CarInsPolicy;
import com.telerikacademy.safetycar.services.serviceContracts.CarInsPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/allRequests")
public class PolicesController {

    private CarInsPolicyService carInsPolicyService;
    private EmailService emailService;

    @Autowired
    public PolicesController(CarInsPolicyService carInsPolicyService, EmailService emailService) {

        this.carInsPolicyService = carInsPolicyService;
        this.emailService = emailService;
    }

    @CrossOrigin
    @GetMapping("")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT')  ")
    public Page<CarInsPolicy> getAllRequest(@RequestParam int page) {
        try {
            return carInsPolicyService.findAll(page);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @CrossOrigin
    @GetMapping("/byStatus")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT')  ")
   public Page<CarInsPolicy> getAllByStatus(@RequestParam String status,@RequestParam int page) {
  // public List<CarInsPolicy> getAllByStatus(@RequestParam String status) {
        try {
            return carInsPolicyService.findAllByPolicyState(status,page);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @CrossOrigin
    @PutMapping("/approve/{requestId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT')  ")
    public Optional<CarInsPolicy> approveRequestById(@PathVariable long requestId) throws Exception {

        // try {
        Optional<CarInsPolicy> approvedPolicy = carInsPolicyService.approveRequestById(requestId);
        if (!approvedPolicy.isPresent()) {

        }
            emailService.sendApprovedEmail(approvedPolicy.get().getContact().getPolicyEmail());

            return approvedPolicy;

    }

    @CrossOrigin
    @PutMapping("/decline/{requestId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT')  ")
    public Optional<CarInsPolicy> declineRequestById(@PathVariable long requestId) throws Exception {

        try {
            Optional<CarInsPolicy> declinedPolicy = carInsPolicyService.declineRequestById(requestId);
            if (declinedPolicy.isPresent()) {
                emailService.sendDeclinedEmail(declinedPolicy.get().getContact().getPolicyEmail());
            }
            return declinedPolicy;
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @CrossOrigin
    @GetMapping("/findByPhoneNumber")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT')  ")
    public List<CarInsPolicy> getAllByPhoneNumber(@RequestParam String phoneNumber) {
        try {
            return carInsPolicyService.findAllByPhoneNumber(phoneNumber);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @CrossOrigin
    @GetMapping("/findByEmail")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT')  ")
    public List<CarInsPolicy> getAllByEmail(@RequestParam String email) {
        try {
            return carInsPolicyService.findAllByPolicyUserEmail(email);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @CrossOrigin
    @GetMapping("/findByRequestDate")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT')  ")
    public List<CarInsPolicy> getAllByRequestDate(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                                  @RequestParam  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate) {
        try {
            return carInsPolicyService.findAllByRequestDate(fromDate,toDate);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

}
