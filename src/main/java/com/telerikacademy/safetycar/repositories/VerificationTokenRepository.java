package com.telerikacademy.safetycar.repositories;


import com.telerikacademy.safetycar.models.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {

        VerificationToken findByToken(String token);


    }
