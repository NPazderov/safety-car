package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.ContactInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ContactInfoRepository extends JpaRepository<ContactInfo,Long> {

}
