package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CarRepository extends JpaRepository<Car,Long> {
    @Query(value="select * from cars as c where c.make=:make",nativeQuery = true)
    List<Car> findAllModelsByGivenMake(@Param(value="make") String make);

    @Query(value="select * from cars as c where c.model=:model",nativeQuery = true)
    List<Car> findAllModelsByGivenModel(@Param(value="model") String model);
}
