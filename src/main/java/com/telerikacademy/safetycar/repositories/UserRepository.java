package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByEmail(String email);

     User findByEmail(String email);

//    @Transactional
//    void deleteByEmail(String username);
}
