package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmount;
import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmountTemp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;


public interface CarBaseAmountRepository extends JpaRepository<CarBaseAmount, Integer> {
    //TODO BigDecimal

    @Query(value = "select base_amount from car_base_amount as ba " +
            "where ba.car_age_min <= :carAge " +
            "and ba.car_age_max >= :carAge  " +
            "and ba.cc_min <= :cubicCapacity " +
            "and ba.cc_max >= :cubicCapacity " +
            "and ba.table_state = 'ACTIVE'", nativeQuery = true)
    double findByCarAgeAndCubicCapacity(@Param("carAge") int carAge, @Param("cubicCapacity") int cubicCapacity);

    @Modifying
    @Transactional
    @Query(value = "insert into car_base_amount " +
            "(cc_min, cc_max,car_age_min,car_age_max,base_amount,table_state,upload_date)" +
            "select t.cc_min,t.cc_max,t.car_age_min,t.car_age_max,t.base_amount,t.table_state,t.upload_date " +
            "from car_base_amount_temp as t " , nativeQuery = true)
    void saveTempTableToOriginal();

    @Modifying
    @Transactional
    @Query(value = "update car_base_amount  " +
            "set table_state = 'INACTIVE' " +
            "where table_state = 'ACTIVE' ", nativeQuery = true)
    void setInactiveOldTable();

    @Query(value = "select distinct c.upload_date from car_base_amount as c " +
            "where c.table_state like 'ACTIVE'" , nativeQuery = true)
    LocalDate findActiveRateDate();

    @Query(value = "select * from car_base_amount as c where c.table_state like 'ACTIVE'", nativeQuery = true)
    List<CarBaseAmount> findAllByTableStateActive();

}
