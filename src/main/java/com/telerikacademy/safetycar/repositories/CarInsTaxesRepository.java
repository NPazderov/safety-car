package com.telerikacademy.safetycar.repositories;


import com.telerikacademy.safetycar.models.CoefficientTables.CarInsTaxes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CarInsTaxesRepository extends JpaRepository<CarInsTaxes, Integer> {

    @Query(value = "select tax_amount from car_ins_taxes as cc " +
            "where cc.table_state='ACTIVE'" ,nativeQuery = true)
    double findTax();

    @Modifying
    @Transactional
    @Query(value = "update car_ins_taxes  " +
            "set table_state = 'INACTIVE' " +
            "where table_state = 'ACTIVE' ", nativeQuery = true)
    void setInactiveOldTable();


    @Modifying
    @Transactional
    @Query(value = "insert into car_ins_taxes " +
            "(tax_amount, table_state,upload_date) " +
            "select t.tax_amount,t.table_state,t.upload_date " +
            "from car_ins_taxes_temp as t " , nativeQuery = true)
    void saveTempTableToOriginal();


    @Query( value="select * from car_ins_taxes as ct where ct.table_state like 'ACTIVE'",nativeQuery = true)
    List<CarInsTaxes> findAllByTableStateActive();




}
