package com.telerikacademy.safetycar.repositories;


import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmountTemp;
import org.springframework.data.jpa.repository.JpaRepository;
public interface CarBaseAmountTempRepository extends JpaRepository<CarBaseAmountTemp, Integer> {

}
