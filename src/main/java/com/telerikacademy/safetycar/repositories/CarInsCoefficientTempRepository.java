package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.CoefficientTables.CarInsCoefficientTemp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarInsCoefficientTempRepository extends JpaRepository<CarInsCoefficientTemp, Integer> {

}
