package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.CoefficientTables.CarInsTaxesTemp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarInsTaxesTempRepository extends JpaRepository<CarInsTaxesTemp, Integer> {


}
