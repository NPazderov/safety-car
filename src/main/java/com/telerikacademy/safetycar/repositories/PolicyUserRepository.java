package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.PolicyUser;

import org.springframework.data.jpa.repository.JpaRepository;


public interface PolicyUserRepository extends JpaRepository<PolicyUser, Long> {

}
