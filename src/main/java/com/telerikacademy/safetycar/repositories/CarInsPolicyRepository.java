package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.CarInsPolicy;
import com.telerikacademy.safetycar.models.Enums.PolicyState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;


public interface CarInsPolicyRepository extends JpaRepository<CarInsPolicy, Long> {

    @Query(value = "select * from car_ins_policy order by quoteDate desc ,startPolicyDate desc, premium desc ", nativeQuery = true)
//    List<CarInsPolicy> findAll();
    Page<CarInsPolicy> findAll(Pageable pageable);

    @Query(value = "select * from car_ins_policy as cp join users as u on u.id=cp.user_id where u.email=:email order by quoteDate desc ,startPolicyDate desc, premium desc ", nativeQuery = true)
    List<CarInsPolicy> findAllByUserEmail(@Param("email") String email);

    @Query(value = "select * from car_ins_policy as cp where cp.policyState=:policyState order by quoteDate desc ,startPolicyDate desc, premium desc", nativeQuery = true)
    Page<CarInsPolicy> findAllByPolicyState(@Param("policyState") String status,Pageable pageable);

    @Query(value = "select * from car_ins_policy as cip join contacts as c on cip.contact_id = c.id where c.phoneNumber=:phoneNumber order by quoteDate desc ,startPolicyDate desc, premium desc ", nativeQuery = true)
    List<CarInsPolicy> findAllByPhoneNumber(@Param(value = "phoneNumber") String phoneNumber);

    @Query(value = "select * from car_ins_policy as cip join contacts as c on cip.contact_id = c.id where c.policyEmail=:policyEmail order by quoteDate desc ,startPolicyDate desc, premium desc", nativeQuery = true)
    List<CarInsPolicy> findAllByPolicyUserEmail(@Param(value = "policyEmail") String email);

    @Query(value = "select * from car_ins_policy as p " +
            "where p.quoteDate between :fromDate and :toDate order by quoteDate desc ,startPolicyDate desc, premium desc", nativeQuery = true)
    List<CarInsPolicy> findAllByRequestDate(@Param(value = "fromDate") LocalDate fromDate, @Param(value = "toDate") LocalDate toDate);

    //  Page<CarInsPolicy> findAll(Pageable pageable);

}
