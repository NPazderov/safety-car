package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.CoefficientTables.CarInsCoefficient;
import com.telerikacademy.safetycar.models.Enums.TableState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

public interface CarInsCoefficientRepository extends JpaRepository<CarInsCoefficient, Integer> {

    @Query(value = "select total_coefficient from car_ins_coefficient as cc" +
            " where  cc.has_accident = :acc"+
            " and  cc.age_under_25 = :underAge " +
            "and cc.table_state='ACTIVE' ",nativeQuery = true)
    Double findByAgeUnder25AndAccident(@Param("underAge")boolean  underAge, @Param("acc")boolean hasAccident);

    @Modifying
    @Transactional
    @Query(value = "insert into car_ins_coefficient" +
            "(age_under_25, has_accident,total_coefficient,table_state,upload_date) " +
            "select t.age_under_25,t.has_accident,t.total_coefficient,t.table_state,t.upload_date " +
            "from car_ins_coefficient_temp as t " , nativeQuery = true)
    void saveTempTableToOriginal();

    @Modifying
    @Transactional
    @Query(value = "update car_ins_coefficient  " +
            "set table_state = 'INACTIVE' " +
            "where table_state = 'ACTIVE' ", nativeQuery = true)
    void setInactiveOldTable();

    @Query(value= "select * from car_ins_coefficient as cc where cc.table_state like 'ACTIVE'",nativeQuery = true)
    List<CarInsCoefficient> findAllByTableStateActive();








}
