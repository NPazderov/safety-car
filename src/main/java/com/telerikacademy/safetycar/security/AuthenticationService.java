package com.telerikacademy.safetycar.security;


import com.telerikacademy.safetycar.utils.email.EmailService;
import com.telerikacademy.safetycar.exceptions.CustomException;
import com.telerikacademy.safetycar.models.DTOs.AuthenticationUserDTO;
import com.telerikacademy.safetycar.models.Enums.Role;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.VerificationToken;
import com.telerikacademy.safetycar.repositories.UserRepository;
import com.telerikacademy.safetycar.repositories.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class AuthenticationService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private EmailService emailService;


    public String login(AuthenticationUserDTO user) {

        try {

            String username = user.getEmail();
            String password = user.getPassword();

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

           if(userRepository.findByEmail(username).isEnabled()) {
               return jwtTokenProvider.createToken(username, userRepository.findByEmail(username).getRole());
           }else {
               throw new CustomException("You must confirm you account from your email first.", HttpStatus.UNPROCESSABLE_ENTITY);
           }
        } catch (AuthenticationException e) {
            throw new CustomException("Invalid email or password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public String signup(User user) throws Exception {


        String password = passwordEncoder.encode(user.getPassword());
        user.setPassword(password);
        user.setRole(Role.ROLE_CLIENT);
        userRepository.save(user);


        VerificationToken verificationToken = new VerificationToken(user);

        verificationTokenRepository.save(verificationToken);

//        if (!userRepository.existsByEmail(user.getEmail())) {
        // !!!!
        emailService.sendConfirmationMail(user.getEmail(), verificationToken);

        return "Successfully";
        //  return user;
//    } else
//
//    {
//        throw new CustomException("email is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
//    }

    }

    public User whoami(HttpServletRequest req) {
        return userRepository.findByEmail(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
    }


    public String refresh(String email) {
        return jwtTokenProvider.createToken(email, userRepository.findByEmail(email).getRole());
    }

    public String confirmUserAccount(String verificationToken, HttpServletResponse response) throws IOException {
        VerificationToken token = verificationTokenRepository.findByToken(verificationToken);

        if (token != null) {
            User user = userRepository.findByEmail(token.getUser().getEmail());
            user.setEnabled(true);
            userRepository.save(user);
            verificationTokenRepository.delete(token);
            response.sendRedirect("http://127.0.0.1:5500/registerVerify.html");
            return "";
        } else {
            return " The link is invalid or broken!";
        }
    }

}