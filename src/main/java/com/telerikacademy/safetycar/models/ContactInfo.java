package com.telerikacademy.safetycar.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "contacts")
public class ContactInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    private String town;

    @NotNull
    private String postalCode;

    @NotNull
    private String address;

    @NotNull
    private String phoneNumber;

    @NotNull
    private String policyEmail;

    public ContactInfo() {
    }

    public ContactInfo(String town, String postalCode, String address, String phoneNumber,
                       String policyEmail) {
        this.town = town;
        this.postalCode = postalCode;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.policyEmail = policyEmail;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public String getPolicyEmail() {
        return policyEmail;
    }

    public void setPolicyEmail(String policyEmail) {
        this.policyEmail = policyEmail;
    }
}
