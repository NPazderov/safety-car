package com.telerikacademy.safetycar.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "cars")
public class Car {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="car_id")
    private Integer id;

    private String make;

    private String model;

    private int cubicCapacity;


    public Car() {
    }

    public Car(String make, String model, int cubicCapacity) {
        this.make = make;
        this.model = model;
        this.cubicCapacity = cubicCapacity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }
}
