package com.telerikacademy.safetycar.models.Enums;

public enum PolicyState {
    PENDING,
    APPROVED,
    DECLINED,
    UNACTIVE;
}
