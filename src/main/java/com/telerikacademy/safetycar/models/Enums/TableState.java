package com.telerikacademy.safetycar.models.Enums;

public enum TableState {
    ACTIVE,
    INACTIVE;
}
