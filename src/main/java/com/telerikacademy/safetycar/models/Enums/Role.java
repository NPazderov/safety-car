package com.telerikacademy.safetycar.models.Enums;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {

    ROLE_ADMIN,
    ROLE_CLIENT,
    ROLE_AGENT;

    public String getAuthority() {
        return name();
    }

}