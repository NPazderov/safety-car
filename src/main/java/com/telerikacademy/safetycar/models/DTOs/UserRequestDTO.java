package com.telerikacademy.safetycar.models.DTOs;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;


public class UserRequestDTO {

    @Email
    private String email;
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,20}$",
            message = "Please enter password that contains:\n" +
                    "\n" +
                    "At least one upper case English letter,\n" +
                    "At least one lower case English letter,\n" +
                    "At least one digit,\n" +
                    "Minimum 8 in length and maximum 20")
    private String password;
    @Size(min = 3, max = 20, message = "Please enter first name between 3 and 20 symbols")
    @Pattern(regexp = "^[A-Za-z]+$",message = "Please enter first name that contains only letters")
    private String firstName;
    @Size(min = 3, max = 20, message = "Please enter last name between 3 and 20 symbols")
    @Pattern(regexp = "^[A-Za-z]+$",message = "Please enter last name that contains only letters")
    private String lastName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


}