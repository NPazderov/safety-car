package com.telerikacademy.safetycar.models.DTOs;

import com.telerikacademy.safetycar.models.Car;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class CarInsPolicyDTO {

    private Car car;

    @Size(min = 3, max = 20, message = "Please enter first name between 3 and 20 symbols")
    @Pattern(regexp = "^[A-Za-z]+$",message = "Please enter first name that contains only letters")
    private String firstName;

    @Size(min = 3, max = 20, message = "Please enter last name between 3 and 20 symbols")
    @Pattern(regexp = "^[A-Za-z]+$",message = "Please enter last name that contains only letters")
    private String lastName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate firstRegistrationDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;

    private boolean accidents;

    private double premium;

    private MultipartFile carCertificateImage;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startPolicyDate;

    @Size(min = 3, max = 20, message = "Please enter town name between 3 and 20 symbols")
    @Pattern(regexp = "^[A-Za-z]+$",message = "Please enter correct town name")
    private String town;

    @Size(min = 3, max = 6, message = "Please enter postal code between 4 and 6 symbols")
    @Pattern(regexp = "^[0-9]+$",message = "Please enter correct postal code name")
        private String postalCode;

    @Size(min = 3, max = 55, message = "Please enter address")
    private String address;

    @Size(min = 6, max = 16, message = "Please enter phone number")
    @Pattern(regexp = "^[0-9]+$",message = "Please enter correct phone number")
    private String phone;

    @Email
    private String policyEmail;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate rateDate;


    public CarInsPolicyDTO() {
    }

    public CarInsPolicyDTO(Car car, String firstName, String lastName, LocalDate firstRegistrationDate,
                           LocalDate dateOfBirth, boolean accidents, double premium,
                           MultipartFile carCertificateImage, LocalDate startPolicyDate,
                           String town, String postalCode, String address, String phone, String policyEmail, LocalDate rateDate) {
        this.car = car;
        this.firstName = firstName;
        this.lastName = lastName;
        this.firstRegistrationDate = firstRegistrationDate;
        this.dateOfBirth = dateOfBirth;
        this.accidents = accidents;
        this.premium = premium;
        this.carCertificateImage = carCertificateImage;
        this.startPolicyDate = startPolicyDate;
        this.town = town;
        this.postalCode = postalCode;
        this.address = address;
        this.phone = phone;
        this.policyEmail = policyEmail;
        this.rateDate=rateDate;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public LocalDate getFirstRegistrationDate() {
        return firstRegistrationDate;
    }

    public void setFirstRegistrationDate(LocalDate firstRegistrationDate) {
        this.firstRegistrationDate = firstRegistrationDate;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isAccidents() {
        return accidents;
    }

    public void setAccidents(boolean accidents) {
        this.accidents = accidents;
    }

    public double getPremium() {
        return premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }

    public MultipartFile getCarCertificateImage() {
        return carCertificateImage;
    }

    public void setCarCertificateImage(MultipartFile carCertificateImage) {
        this.carCertificateImage = carCertificateImage;
    }

    public LocalDate getStartPolicyDate() {
        return startPolicyDate;
    }

    public void setStartPolicyDate(LocalDate startPolicyDate) {
        this.startPolicyDate = startPolicyDate;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPolicyEmail() {
        return policyEmail;
    }

    public void setPolicyEmail(String policyEmail) {
        this.policyEmail = policyEmail;
    }

    public LocalDate getRateDate() {
        return rateDate;
    }

    public void setRateDate(LocalDate rateDate) {
        this.rateDate = rateDate;
    }
}