package com.telerikacademy.safetycar.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "policy_users")
public class PolicyUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;


    public PolicyUser() {
    }

    public PolicyUser( String firstName,
                      String lastName) {

        this.firstName = firstName;
        this.lastName = lastName;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}

