package com.telerikacademy.safetycar.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.safetycar.models.Enums.PolicyState;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "car_ins_policy")
public class CarInsPolicy {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "car_id")
    private Car car;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @NotNull
    private LocalDate firstRegistrationDate;

    @NotNull
    private String carCertificateImage;

    @NotNull
    private double premium;

    @NotNull
    private boolean accidents;

    @NotNull
    private LocalDate startPolicyDate;

    @NotNull
    private LocalDateTime quoteDate;

    @Enumerated(EnumType.STRING)
    private PolicyState policyState;

    private int driverAge;

    @NotNull
    @ManyToOne
    private ContactInfo contact;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "policy_user_id")
    private PolicyUser policyUser;

    @NotNull
    private LocalDate rateDate;

    public CarInsPolicy() {
    }

    public CarInsPolicy(Car car, User user, LocalDate firstRegistrationDate, String carCertificateImage, double premium,
                        boolean accidents, LocalDate startPolicyDate, LocalDateTime quoteDate, PolicyState policyState,
                        int driverAge, ContactInfo contact, PolicyUser policyUser, LocalDate rateDate) {
        this.car = car;
        this.user = user;
        this.firstRegistrationDate = firstRegistrationDate;
        this.carCertificateImage = carCertificateImage;
        this.premium = premium;
        this.accidents = accidents;
        this.startPolicyDate = startPolicyDate;
        this.quoteDate = quoteDate;
        this.policyState = policyState;
        this.driverAge = driverAge;
        this.contact = contact;
        this.policyUser = policyUser;
        this.rateDate = rateDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getFirstRegistrationDate() {
        return firstRegistrationDate;
    }

    public void setFirstRegistrationDate(LocalDate firstRegistrationDate) {
        this.firstRegistrationDate = firstRegistrationDate;
    }

    public String getCarCertificateImage() {
        return carCertificateImage;
    }

    public void setCarCertificateImage(String carCertificateImage) {
        this.carCertificateImage = carCertificateImage;
    }

    public double getPremium() {
        return premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }

    public boolean isAccidents() {
        return accidents;
    }

    public void setAccidents(boolean accidents) {
        this.accidents = accidents;
    }

    public LocalDate getStartPolicyDate() {
        return startPolicyDate;
    }

    public void setStartPolicyDate(LocalDate startPolicyDate) {
        this.startPolicyDate = startPolicyDate;
    }

    public LocalDateTime getQuoteDate() {
        return quoteDate;
    }

    public void setQuoteDate(LocalDateTime quoteDate) {
        this.quoteDate = quoteDate;
    }

    public PolicyState getPolicyState() {
        return policyState;
    }

    public void setPolicyState(PolicyState policyState) {
        this.policyState = policyState;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public ContactInfo getContact() {
        return contact;
    }

    public void setContact(ContactInfo contact) {
        this.contact = contact;
    }

    public PolicyUser getPolicyUser() {
        return policyUser;
    }

    public void setPolicyUser(PolicyUser policyUser) {
        this.policyUser = policyUser;
    }

    public LocalDate getRateDate() {
        return rateDate;
    }

    public void setRateDate(LocalDate rateDate) {
        this.rateDate = rateDate;
    }
}
