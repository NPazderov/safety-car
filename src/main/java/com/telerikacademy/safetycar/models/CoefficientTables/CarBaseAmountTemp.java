package com.telerikacademy.safetycar.models.CoefficientTables;

import com.telerikacademy.safetycar.models.Enums.TableState;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "car_base_amount_temp")
public class CarBaseAmountTemp {

    @Id
    @Column(name="id")
    private int id;

    @Column(name="cc_min")
    private int cubicCapacityMin;

    @Column(name="cc_max")
    private int cubicCapacityMax;

    @Column(name="car_age_min")
    private int carAgeMin;

    @Column(name="car_age_max")
    private int carAgeMax;

    @Column(name="base_amount")
    private double baseAmount;

    @Enumerated(EnumType.STRING)
    @Column(name="table_state")
    private TableState tableState;

    @Column(name="upload_date")
    private LocalDate dateOfActivation;

    public CarBaseAmountTemp() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCubicCapacityMin() {
        return cubicCapacityMin;
    }

    public void setCubicCapacityMin(int cubicCapacityMin) {
        this.cubicCapacityMin = cubicCapacityMin;
    }

    public int getCubicCapacityMax() {
        return cubicCapacityMax;
    }

    public void setCubicCapacityMax(int cubicCapacityMax) {
        this.cubicCapacityMax = cubicCapacityMax;
    }

    public int getCarAgeMin() {
        return carAgeMin;
    }

    public void setCarAgeMin(int carAgeMin) {
        this.carAgeMin = carAgeMin;
    }

    public int getCarAgeMax() {
        return carAgeMax;
    }

    public void setCarAgeMax(int carAgeMax) {
        this.carAgeMax = carAgeMax;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    public TableState getTableState() {
        return tableState;
    }

    public void setTableState(TableState tableState) {
        this.tableState = tableState;
    }

    public LocalDate getDateOfActivation() {
        return dateOfActivation;
    }

    public void setDateOfActivation(LocalDate dateOfActivation) {
        this.dateOfActivation = dateOfActivation;
    }
}


