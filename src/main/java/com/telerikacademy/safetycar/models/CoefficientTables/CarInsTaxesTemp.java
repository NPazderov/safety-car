package com.telerikacademy.safetycar.models.CoefficientTables;

import com.telerikacademy.safetycar.models.Enums.TableState;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "car_ins_taxes_temp")
public class CarInsTaxesTemp {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "tax_amount")
    private double taxAmount;

    @Enumerated(EnumType.STRING)
    @Column(name="table_state")
    private TableState tableState;

    @Column(name="upload_date")
    private LocalDate dateOfActivation;

    public CarInsTaxesTemp() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public TableState getTableState() {
        return tableState;
    }

    public void setTableState(TableState tableState) {
        this.tableState = tableState;
    }

    public LocalDate getDateOfActivation() {
        return dateOfActivation;
    }

    public void setDateOfActivation(LocalDate dateOfActivation) {
        this.dateOfActivation = dateOfActivation;
    }
}























