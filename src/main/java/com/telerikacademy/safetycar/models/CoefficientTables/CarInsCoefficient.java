package com.telerikacademy.safetycar.models.CoefficientTables;

import com.telerikacademy.safetycar.models.Enums.TableState;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "car_ins_coefficient")
public class CarInsCoefficient {

    @Id
    @Column(name="id")
    private int id;

    @Column(name="age_under_25")
    private int ageUnder25;

    @Column(name="has_accident")
    private int accident;

    @Column(name="total_coefficient")
    private double totalCoefficient;

    @Enumerated(EnumType.STRING)
    @Column(name="table_state")
    private TableState tableState;

    @Column(name="upload_date")
    private LocalDate dateOfActivation;

    public CarInsCoefficient() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAgeUnder25() {
        return ageUnder25;
    }

    public void setAgeUnder25(int ageUnder25) {
        this.ageUnder25 = ageUnder25;
    }

    public int getAccident() {
        return accident;
    }

    public void setAccident(int accident) {
        this.accident = accident;
    }

    public double getTotalCoefficient() {
        return totalCoefficient;
    }

    public void setTotalCoefficient(double totalCoefficient) {
        this.totalCoefficient = totalCoefficient;
    }

    public TableState getTableState() {
        return tableState;
    }

    public void setTableState(TableState tableState) {
        this.tableState = tableState;
    }

    public LocalDate getDateOfActivation() {
        return dateOfActivation;
    }

    public void setDateOfActivation(LocalDate dateOfActivation) {
        this.dateOfActivation = dateOfActivation;
    }
}


