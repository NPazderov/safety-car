package com.telerikacademy.safetycar.utils.fileStorage;

import com.telerikacademy.safetycar.models.DTOs.UploadFileResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Objects;

@Service
public class ImageServiceImpl implements ImageService {

    private FileStorageService fileStorageService;

    @Autowired
    public ImageServiceImpl(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    public UploadFileResponseDTO saveImage(MultipartFile file) {

        if(isFileInvalid(file)){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "File is not jpg or png!");
        }

        String fileName = fileStorageService.storeFile(file);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/images/downloadFile/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponseDTO(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    private boolean isFileInvalid(MultipartFile file) {
        return !Objects.equals(file.getContentType(), "image/jpeg") &&
                !Objects.equals(file.getContentType(), "image/png");
    }





}