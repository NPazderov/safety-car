package com.telerikacademy.safetycar.utils.fileStorage;

import com.telerikacademy.safetycar.models.DTOs.UploadFileResponseDTO;
import org.springframework.web.multipart.MultipartFile;


public interface ImageService {

    UploadFileResponseDTO saveImage(MultipartFile file);

}
