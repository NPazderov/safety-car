package com.telerikacademy.safetycar.utils.email;//package email;

import com.telerikacademy.safetycar.exceptions.EmailNotSentException;
import com.telerikacademy.safetycar.models.VerificationToken;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Properties;

@Component
public class EmailServiceImpl implements EmailService {

    private static final String myAccountEmail = "safetycartest@gmail.com";
    private static final String password = "safetycar123";

    public Message prepareMessage(String recepient, VerificationToken token) {

        Session session = mailProperties();

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(myAccountEmail));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
            message.setSubject("Verify your account, Secure your Car ! :)");
            message.setText("Thank you for joining our Safety Car Community \n" +
                    "To confirm your account, please click here : \n"
                    + "http://localhost:8080/api/auth/confirm-account?token=" + token.getToken());

            return message;
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void sendConfirmationMail(String recepient, VerificationToken token) throws Exception {

        Message message = prepareMessage(recepient, token);
        Transport.send(message);
        System.out.println("Message sent successfully!");


    }

    public void sendApprovedEmail(String recepient)  {

        String subject = "Safety Car - Approved Request";
        String text = "Thank you for joining our Safety Car Community! \n" +
                " Your policy was approved . \n "
                + " We will contact you with additional information.";

        sendMessage(recepient,subject,text);
    }

    public void sendDeclinedEmail(String recepient)  {

        String subject = "Safety Car - Declined Request";
        String text = "Sorry, your policy was declined ! \n " +
                " We will contact you with additional information";

        sendMessage(recepient,subject,text);
    }



    public void sendMessage(String recepient,String subject,String text){

        Session session = mailProperties();

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(myAccountEmail));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
            message.setSubject(subject);
            message.setText(text);

            Transport.send(message);
            System.out.println("Message sent successfully!");

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private Session mailProperties() {

        Properties properties = new Properties();

        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        return Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(myAccountEmail, password);
            }
        });
    }

    public Message PDFMessage(String recepient, String path) throws IOException {

        Session session = mailProperties();
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(myAccountEmail));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
            message.setSubject("Get your Policy in PDF Format");

            MimeMultipart mimeMultipart = new MimeMultipart();

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setText("Here is your policy. Download and review");

            MimeBodyPart pdfAttachment = new MimeBodyPart();

            pdfAttachment.attachFile(path);

            mimeMultipart.addBodyPart(mimeBodyPart);
            mimeMultipart.addBodyPart(pdfAttachment);

            message.setContent(mimeMultipart);

            return message;
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void sendMessageWithAttachment(String recepient, String path) throws Exception {
        Message message = PDFMessage(recepient, path);

        try {
            Transport.send(message);
        } catch (Exception ex) {
            throw new EmailNotSentException("PDF was not sent");

        }
        System.out.println("Message sent successfully!");
    }
}