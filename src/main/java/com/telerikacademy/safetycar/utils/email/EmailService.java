package com.telerikacademy.safetycar.utils.email;//package email;

import com.telerikacademy.safetycar.models.VerificationToken;

import javax.mail.Message;
import java.io.IOException;

public interface EmailService {


   void sendConfirmationMail(String recepient, VerificationToken token) throws Exception;

   Message prepareMessage( String recepient, VerificationToken token) throws Exception;

   void sendApprovedEmail(String email) throws Exception ;

    void sendDeclinedEmail(String recepient) throws Exception;

    void sendMessage(String recepient,String subject,String text);

    Message PDFMessage(String recepient, String path) throws IOException;

    void sendMessageWithAttachment(String email,String path) throws Exception;
}

