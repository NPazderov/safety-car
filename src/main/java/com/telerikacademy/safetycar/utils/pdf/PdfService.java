package com.telerikacademy.safetycar.utils.pdf;

import com.itextpdf.text.DocumentException;
import com.telerikacademy.safetycar.models.User;

import java.io.FileNotFoundException;

public interface PdfService {

    String printPDF(User loggedUser, long policyId) throws FileNotFoundException, DocumentException;
}
