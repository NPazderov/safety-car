package com.telerikacademy.safetycar.utils.excelTablesReaderWriter;

import com.telerikacademy.safetycar.services.serviceContracts.CarBaseAmountService;
import com.telerikacademy.safetycar.services.serviceContracts.CarInsCofficientService;
import com.telerikacademy.safetycar.services.serviceContracts.CarInsTaxesService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

@Service
public class ExcelTableServiceImpl implements ExcelTableService {


    private CarInsTaxesService carInsTaxesService;
    private CarInsCofficientService carInsCofficientService;
    private CarBaseAmountService carBaseAmountService;

    @Autowired
    public ExcelTableServiceImpl(CarInsTaxesService carInsTaxesService, CarInsCofficientService carInsCofficientService,
                                 CarBaseAmountService carBaseAmountService) {
        this.carInsTaxesService = carInsTaxesService;
        this.carInsCofficientService = carInsCofficientService;
        this.carBaseAmountService = carBaseAmountService;
    }


    public String readAndWriteToDatabase(MultipartFile file) throws IOException {

        // Creating a Workbook from an Excel file (.xls or .xlsx)

        InputStream inputStream = file.getInputStream();

        Workbook workbook;
        if (file.getOriginalFilename().endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (file.getName().endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        System.out.println("Following " + workbook.getNumberOfSheets() + " sheets will be write in database :");
        //  System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

        DataFormatter dataFormatter = new DataFormatter();

        for (Sheet sheet : workbook) {

            System.out.println();
            System.out.println("*** " + sheet.getSheetName() + " ***");
            System.out.println();

            String sheetName = sheet.getSheetName();

            for (Row row : sheet) {
                ArrayList<String> inputLine = new ArrayList<>();
                for (Cell cell : row) {
                    String cellValue = dataFormatter.formatCellValue(cell);
                    System.out.print(cellValue + "\t");
                    if (row.getRowNum() == 0) {
                        continue; //just skip the rows
                    }
                    inputLine.add(cellValue);
                }

                if (sheetName.equals("Book1_base_amount") && row.getRowNum() != 0) {
                    carBaseAmountService.saveTempTable(inputLine);
                }


                if (sheetName.equals("Book2_total_ins_coefficient") && row.getRowNum() != 0) {
                    carInsCofficientService.saveTempTable(inputLine);
                }

                if (sheetName.equals("Book3_tax_amount") && row.getRowNum() != 0) {
                    carInsTaxesService.saveTempTable(inputLine);
                }
                System.out.println();
            }
        }
        workbook.close();

// TO DO validations one day ....


        carBaseAmountService.setAllRowsAsInactive();
        carBaseAmountService.saveTempTableToOriginal();
        carBaseAmountService.deleteTempTable();

        carInsCofficientService.setAllRowsAsInactive();
        carInsCofficientService.saveTempTableToOriginal();
        carInsCofficientService.deleteTempTable();

        carInsTaxesService.setAllRowsAsInactive();
        carInsTaxesService.saveTempTableToOriginal();
        carInsTaxesService.deleteTempTable();

        return " Successful uploaded to database!";

    }
}