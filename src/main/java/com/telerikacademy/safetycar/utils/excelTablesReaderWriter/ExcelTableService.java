package com.telerikacademy.safetycar.utils.excelTablesReaderWriter;


import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;

public interface ExcelTableService {

    String readAndWriteToDatabase(MultipartFile file) throws IOException;
}
