
package com.telerikacademy.safetycar.Services;

import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.repositories.*;
import com.telerikacademy.safetycar.services.GetQuoteServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import java.time.LocalDate;

@RunWith(MockitoJUnitRunner.Silent.class)

public class GetQuoteServiceTests {

      @Mock
    CarBaseAmountRepository mockCarBaseAmountRepo;

    @Mock
    CarInsCoefficientRepository mockCarInsCoefficentRepository;

    @Mock
    CarInsTaxesRepository mockCarInsTaxesRepository;

    @InjectMocks
    GetQuoteServiceImpl getQuoteService;


    private CarInsPolicyDTO carDto() {

        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());

        return new CarInsPolicyDTO(car(), "firstName", "lastName", LocalDate.parse("2019-02-02"),
                LocalDate.parse("1990-01-01"), true, 123.45,
                firstFile, LocalDate.now(), "Sofia", "1000", "Adress", "9785236522",
                "client@email.com", LocalDate.parse("2000-10-10"));
    }


    private Car car() {

        return new Car("Audi", "A4", 1600);
    }

    @Test
    public void calculateTotalPremium_Should_Return_Double_If_Data_Correct() {

        CarInsPolicyDTO carInsPolicyDTO = carDto();

        double netPremium = 100 * 1.2;
        double totalTaxes = 0.1 * 100 * 1.2;
        double tempResult = netPremium + totalTaxes;

        Mockito.when(mockCarBaseAmountRepo.findByCarAgeAndCubicCapacity(0,1600))
                .thenReturn(100.0);
        Mockito.when(mockCarInsCoefficentRepository.findByAgeUnder25AndAccident(false,true))
                .thenReturn(1.2);
        Mockito.when(mockCarInsTaxesRepository.findTax())
                .thenReturn(0.1);

        double result = getQuoteService.calculateTotalPremium(carInsPolicyDTO);
        System.out.println(result);

        Assert.assertEquals(tempResult, result,0);

    }


}
