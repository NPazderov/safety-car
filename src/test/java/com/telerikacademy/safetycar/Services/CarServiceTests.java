
package com.telerikacademy.safetycar.Services;

import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.repositories.CarRepository;
import com.telerikacademy.safetycar.services.CarServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.Silent.class)

public class CarServiceTests {

    @Mock
    CarRepository mockCarRepo;

    @InjectMocks
    CarServiceImpl carService;


    private CarInsPolicyDTO carDto() {

        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());

        return new CarInsPolicyDTO(car(), "firstName", "lastName", LocalDate.parse("2019-02-02"),
                LocalDate.parse("1990-01-01"), true, 123.45,
                firstFile, LocalDate.now(), "Sofia", "1000", "Adress", "9785236522",
                "client@email.com", LocalDate.parse("2000-10-10"));
    }


    private Car car() {

        return new Car("Audi", "A4", 1600);
    }

    @Test
    public void findAllCars_Should_Return_All_Cars_From_Repo() {

        //Arrange
        Car car1 = car();
        Car car2 = car();
        List<Car> cars= new ArrayList<>();
        cars.add(car1);
        cars.add(car2);

        Mockito.when(mockCarRepo.findAll()).thenReturn(cars);
        //Act
        List<Car> result = carService.findAll();

        //Assert
        Assert.assertEquals(2,result.size());
    }



    @Test
    public void findAllCars_By_Make_Should_Return_All_Cars_By_GivenMAke_From_Repo() {

        //Arrange
        Car car1 = car();
        car1.setMake("Mazda");
        Car car2 = car();
        car2.setMake("Mazda");

        List<Car> cars= new ArrayList<>();
        cars.add(car1);
        cars.add(car2);

        Mockito.when(mockCarRepo.findAllModelsByGivenMake("Mazda")).thenReturn(cars);
        //Act
        List<Car> result = carService.findAllModelsByGivenMake("Mazda");

        //Assert
        Assert.assertEquals(2,result.size());
    }

    @Test
    public void findAllCars_By_Model_Should_Return_All_Cars_By_GivenModel_From_Repo() {

        //Arrange
        Car car1 = car();
        car1.setModel("Corsa");
        Car car2 = car();
        car1.setModel("Corsa");

        List<Car> cars= new ArrayList<>();
        cars.add(car1);
        cars.add(car2);

        Mockito.when(mockCarRepo.findAllModelsByGivenModel("Corsa")).thenReturn(cars);
        //Act
        List<Car> result = carService.findAllModelsByGivenModel("Corsa");

        //Assert
        Assert.assertEquals(2,result.size());
    }





}
