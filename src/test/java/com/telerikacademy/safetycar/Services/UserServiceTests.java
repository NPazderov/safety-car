
package com.telerikacademy.safetycar.Services;

import com.telerikacademy.safetycar.exceptions.CustomException;
import com.telerikacademy.safetycar.exceptions.EmailExistsException;
import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.DTOs.UserRequestDTO;
import com.telerikacademy.safetycar.models.Enums.PolicyState;
import com.telerikacademy.safetycar.models.Enums.Role;
import com.telerikacademy.safetycar.repositories.CarInsPolicyRepository;
import com.telerikacademy.safetycar.repositories.UserRepository;
import com.telerikacademy.safetycar.services.CarInsPolicyServiceImpl;
import com.telerikacademy.safetycar.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceTests {

    @Mock
    UserRepository mockUserRepo;

    @Mock
    PasswordEncoder mockPassEncoder;

    @InjectMocks
    UserServiceImpl userService;


    private CarInsPolicy tempPendingPolicy() {
        CarInsPolicy pendingPolicy = new CarInsPolicy(car(), client(), LocalDate.parse("2017-11-10"), "index.jpg", 123.45, true, LocalDate.now().plusDays(1),
                LocalDateTime.now().minusDays(1), PolicyState.PENDING, 25, contact(), policyUser(), LocalDate.parse("2016-11-10"));
        pendingPolicy.setId(10L);
        return pendingPolicy;
    }

    private CarInsPolicy tempApprovedPolicy() {

        return new CarInsPolicy(car(), client(), LocalDate.parse("2017-11-10"), "index.jpg", 123.45, true, LocalDate.now().plusDays(1),
                LocalDateTime.now().minusDays(1), PolicyState.APPROVED, 25, contact(), policyUser(), LocalDate.parse("2016-11-10"));
    }

    private CarInsPolicy tempDeclinedPolicy() {

        return new CarInsPolicy(car(), client(), LocalDate.parse("2017-11-10"), "index.jpg", 123.45, true, LocalDate.now().plusDays(1),
                LocalDateTime.now().minusDays(1), PolicyState.DECLINED, 25, contact(), policyUser(), LocalDate.parse("2016-11-10"));
    }

    private PolicyUser policyUser() {

        return new PolicyUser("firstName", "lastName");
    }

    private ContactInfo contact() {

        return new ContactInfo("Varna", "1000", "25 Hajdushko horo.Str", "0889713670", "test@email.com");
    }

    private Car car() {

        return new Car("Audi", "A4", 1600);
    }

    private User admin() {

        User user = new User("emailAdmin@email.com", "password", "firstName", "lastName");
        user.setRole(Role.ROLE_ADMIN);
        user.setId(1L);
        return user;
    }

    private User client() {

        User user = new User("emailClient@email.com", "password", "firstName", "lastName");
        user.setRole(Role.ROLE_CLIENT);
        user.setId(3L);
        return user;
    }


    @Test
    public void findById_Should_ReturnUser_When_MatchExists() {

        //Arrange
        User user = admin();
        user.setId(1L);

        Optional<User> optional = Optional.of(user);
        Mockito.when(mockUserRepo.findById(1L)).thenReturn(optional);

        //Act
        User result = userService.findById(1L);

        //Assert
        Assert.assertNotNull(result);
    }


    @Test(expected = CustomException.class)
    public void findById_Should_ThrowException_When_UserDoesntExists() {
        //Arrange
        long userId = 1L;

        Optional<User> optional = Optional.empty();
        Mockito.when(mockUserRepo.findById(userId)).thenReturn(optional);

        //Act
        User result = userService.findById(userId);

    }

    @Test
    public void findByEmail_should_return_User_with_given_Email_When_Exists() {

        //Arrange
        String email = "admin@email.com";
        User user = admin();
        user.setFirstName("FIRST_NAME");
        user.setEmail(email);


        Mockito.when(mockUserRepo.findByEmail(email)).thenReturn(user);
        //Act
        User result = userService.findByEmail(email);
        result.setFirstName("FIRST_NAME");

        //Assert
        Assert.assertEquals(result.getFirstName(), user.getFirstName());
    }

    @Test(expected = CustomException.class)
    public void findByEmail_should_ThrowException_When_User_DoesntExists() {

        //Arrange
        String email = "TEST@email.com";
        // User user = admin();

        Mockito.when(mockUserRepo.findByEmail(email)).thenReturn(null);

        User result = userService.findByEmail(email);

    }


    @Test
    public void update_User_Should_Return_UpdatedUser_When_Successful() {
        //Arrange
        long userId = 3L;
        User oldUser = client();
        oldUser.setId(3L);

        String lastName = "NewLastName";
        String firstName = "NewFirstName";
        String newEmail = "TEST@email.com";
        String newPassword = "passWord123";

        UserRequestDTO newUser = new UserRequestDTO();
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(newEmail);
        newUser.setPassword(newPassword);

        Optional<User> updatedUser = Optional.of(oldUser);
        Mockito.when(mockUserRepo.findById(3L)).thenReturn(updatedUser);
        updatedUser.get().setFirstName(newUser.getFirstName());
        updatedUser.get().setLastName(newUser.getLastName());
        updatedUser.get().setEmail(newUser.getEmail());
        updatedUser.get().setPassword(mockPassEncoder.encode(newUser.getPassword()));

        Mockito.when(mockUserRepo.save(updatedUser.get())).thenReturn(updatedUser.get());

        //Act
        User result = userService.update(3L, newUser);

        //Assert
        Assert.assertEquals(result, updatedUser.get());
    }

    @Test
    public void getUserRequest_Should_Return_All_PolicesOfUser_If_Has_Any() {

        String emailName = "TEST@email.com";
        User user = client();
        user.setEmail(emailName);

        List<CarInsPolicy> policies = new ArrayList<>();
        policies.add(tempApprovedPolicy());
        policies.add(tempDeclinedPolicy());
        policies.add(tempPendingPolicy());

        user.setUserCarInsPolicy(policies);

        Mockito.when(mockUserRepo.save(user)).thenReturn(user);
        Mockito.when(mockUserRepo.findByEmail(emailName)).thenReturn(user);

        //Act
        List<CarInsPolicy> result = userService.getUserRequests(emailName);

        //Assert
        Assert.assertEquals(result.size(), 3);

    }


}
