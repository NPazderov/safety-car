
package com.telerikacademy.safetycar.Services;

import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.repositories.ContactInfoRepository;
import com.telerikacademy.safetycar.services.ContactInfoServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.Silent.class)

public class ContactInfoServiceTests {

    @Mock
    ContactInfoRepository mockContactInfoRepo;

    @InjectMocks
    ContactInfoServiceImpl contactInfoService;

    private Car car() {

        return new Car("Audi", "A4", 1600);
    }

    private CarInsPolicyDTO carDto() {

        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());

        return new CarInsPolicyDTO(car(), "firstName", "lastName", LocalDate.parse("2019-02-02"),
                LocalDate.parse("1990-01-01"), true, 123.45,
                firstFile, LocalDate.now(), "Sofia", "1000", "Adress", "9785236522",
                "client@email.com", LocalDate.parse("2000-10-10"));
    }


    @Test
    public void save_Policy_Should_Return_Policy_When_Successful() {
        //Arrange
        ContactInfo contactInfo= new ContactInfo();
        CarInsPolicyDTO dto = carDto();

        Mockito.when(mockContactInfoRepo.save(any())).thenReturn(contactInfo);

        //Act
        ContactInfo result = contactInfoService.save(dto);

        //Assert
        Assert.assertNotNull(result);
    }


}
