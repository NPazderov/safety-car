
package com.telerikacademy.safetycar.Services.TablesServices;

import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmount;
import com.telerikacademy.safetycar.models.CoefficientTables.CarBaseAmountTemp;
import com.telerikacademy.safetycar.models.Enums.TableState;
import com.telerikacademy.safetycar.repositories.CarBaseAmountRepository;
import com.telerikacademy.safetycar.repositories.CarBaseAmountTempRepository;
import com.telerikacademy.safetycar.services.tablesAndFilesServices.CarBaseAmountServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.Silent.class)

public class CarBaseAmountServiceTests {

    @Mock
    CarBaseAmountTempRepository mockCarBaseAmountTempRepo;

    @Mock
    CarBaseAmountRepository mockCarBaseAmountRepo;

    @InjectMocks
    CarBaseAmountServiceImpl carBaseAmountService;


    @Test
    public void saveTempTable_Return_Row_From_Table_When_Successful() {
        //Arrange
        CarBaseAmountTemp tempRow = new CarBaseAmountTemp();

        ArrayList<String> inputLine = new ArrayList<>();
        inputLine.add("1");
        inputLine.add("1");
        inputLine.add("1");
        inputLine.add("1");
        inputLine.add("1");
        inputLine.add("1.1");

        Mockito.when(mockCarBaseAmountTempRepo.save(any())).thenReturn(tempRow);

        //Act
        CarBaseAmountTemp result = carBaseAmountService.saveTempTable(inputLine);

        //Assert
        Assert.assertNotNull(result);
    }


    @Test(expected = Exception.class)
    public void saveTempTable_Throw_Exception_When_Data_Is_Not_Correct_Type() {
        //Arrange
        CarBaseAmountTemp tempRow = new CarBaseAmountTemp();

        ArrayList<String> inputLine = new ArrayList<>();
        inputLine.add("A");
        inputLine.add("AA");
        inputLine.add("300");
        inputLine.add("10");
        inputLine.add("20");
        inputLine.add("20.2");

        Mockito.when(mockCarBaseAmountTempRepo.save(any())).thenReturn(tempRow);

        //Act
        carBaseAmountService.saveTempTable(inputLine);
    }


    @Test
    public void setAllRows_To_Inactive_InCurrentDB() {
        //Arrange
        CarBaseAmount row1 = new CarBaseAmount();
        row1.setTableState(TableState.ACTIVE);
        CarBaseAmount row2 = new CarBaseAmount();
        row2.setTableState(TableState.ACTIVE);

        List<CarBaseAmount> tempRow =new ArrayList<>();
        tempRow.add(row1);
        tempRow.add(row2);

        Mockito.when(mockCarBaseAmountRepo.findAll()).thenReturn(tempRow);

        //Act
        carBaseAmountService.setAllRowsAsInactive();
        List<CarBaseAmount> result = carBaseAmountService.findAllByTableStateActive();

        //Assert
        Assert.assertEquals(0,result.size());
    }

    public void saveTempTable_To_Original() {

        //Arrange
        CarBaseAmount row1 = new CarBaseAmount();
        CarBaseAmount row2 = new CarBaseAmount();

        List<CarBaseAmount> tempRow =new ArrayList<>();
        tempRow.add(row1);
        tempRow.add(row2);

        Mockito.when(mockCarBaseAmountRepo.findAll()).thenReturn(tempRow);

        //Act
        carBaseAmountService.saveTempTableToOriginal();
        List<CarBaseAmount> result = carBaseAmountService.findAllByTableStateActive();

        //Assert
        Assert.assertEquals(0,result.size());
    }





}
