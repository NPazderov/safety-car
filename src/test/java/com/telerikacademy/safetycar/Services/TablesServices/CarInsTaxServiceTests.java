
package com.telerikacademy.safetycar.Services.TablesServices;

import com.telerikacademy.safetycar.models.CoefficientTables.*;
import com.telerikacademy.safetycar.models.Enums.TableState;
import com.telerikacademy.safetycar.repositories.CarInsCoefficientRepository;
import com.telerikacademy.safetycar.repositories.CarInsCoefficientTempRepository;
import com.telerikacademy.safetycar.repositories.CarInsTaxesRepository;
import com.telerikacademy.safetycar.repositories.CarInsTaxesTempRepository;
import com.telerikacademy.safetycar.services.tablesAndFilesServices.CarInsCoefficientServiceImpl;
import com.telerikacademy.safetycar.services.tablesAndFilesServices.CarInsTaxServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.Silent.class)

public class CarInsTaxServiceTests {

    @Mock
    CarInsTaxesTempRepository mockCarInsTaxesTempRepo;

    @Mock
    CarInsTaxesRepository mockCarInsTaxesRepo;

    @InjectMocks
    CarInsTaxServiceImpl carInsTaxService;

    @Test
    public void saveTempTable_Return_Row_From_Table_When_Successful() {

        //Arrange
        CarInsTaxesTemp tempRow = new CarInsTaxesTemp();

        ArrayList<String> inputLine = new ArrayList<>();
        inputLine.add("1");
        inputLine.add("1.1");

        Mockito.when(mockCarInsTaxesTempRepo.save(any())).thenReturn(tempRow);

        //Act
        CarInsTaxesTemp result = carInsTaxService.saveTempTable(inputLine);

        //Assert
        Assert.assertNotNull(result);
    }


    @Test(expected = Exception.class)
    public void saveTempTable_Throw_Exception_When_Data_Is_Not_Correct_Type() {

        //Arrange
        CarInsTaxesTemp tempRow = new CarInsTaxesTemp();

        ArrayList<String> inputLine = new ArrayList<>();
        inputLine.add("1");
        inputLine.add("B");

        Mockito.when(mockCarInsTaxesTempRepo.save(any())).thenReturn(tempRow);

        //Act
        CarInsTaxesTemp result = carInsTaxService.saveTempTable(inputLine);

    }

    @Test
    public void setAllRows_To_Inactive_InCurrentDB() {

        //Arrange
        CarInsTaxes row1 = new CarInsTaxes();
        row1.setTableState(TableState.ACTIVE);
        CarInsTaxes row2 = new CarInsTaxes();
        row2.setTableState(TableState.ACTIVE);

        List<CarInsTaxes> tempRow =new ArrayList<>();
        tempRow.add(row1);
        tempRow.add(row2);

        Mockito.when(mockCarInsTaxesRepo.findAll()).thenReturn(tempRow);

        //Act
        carInsTaxService.setAllRowsAsInactive();
        List<CarInsTaxes> result = carInsTaxService.findAllByTableStateActive();

        //Assert
        Assert.assertEquals(0,result.size());
    }


    @Test
    public void saveTempTable_To_Original() {

        //Arrange
        CarInsTaxes row1 = new CarInsTaxes();
        CarInsTaxes row2 = new CarInsTaxes();

        List<CarInsTaxes> tempRow =new ArrayList<>();
        tempRow.add(row1);
        tempRow.add(row2);

        Mockito.when(mockCarInsTaxesRepo.findAll()).thenReturn(tempRow);

        //Act
        carInsTaxService.saveTempTableToOriginal();
        List<CarInsTaxes> result = carInsTaxService.findAllByTableStateActive();

        //Assert
        Assert.assertEquals(0,result.size());
    }


}
