
package com.telerikacademy.safetycar.Services.TablesServices;

import com.telerikacademy.safetycar.models.CoefficientTables.*;
import com.telerikacademy.safetycar.models.Enums.TableState;
import com.telerikacademy.safetycar.repositories.CarInsCoefficientRepository;
import com.telerikacademy.safetycar.repositories.CarInsCoefficientTempRepository;
import com.telerikacademy.safetycar.services.tablesAndFilesServices.CarInsCoefficientServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.Silent.class)

public class CarInsCoefficientServiceTests {

      @Mock
      CarInsCoefficientTempRepository mockCarInsCoefficientTempRepository;

    @Mock
    CarInsCoefficientRepository mockCarInsCoefficientRepo;

    @InjectMocks
    CarInsCoefficientServiceImpl carInsCoefficientService;


    @Test(expected = Exception.class)
    public void saveTempTable_Throw_Exception_When_Data_Is_Not_Correct_Type() {
        //Arrange
        CarBaseAmountTemp tempRow = new CarBaseAmountTemp();

        ArrayList<String> inputLine = new ArrayList<>();
        inputLine.add("A");

        Mockito.when(mockCarInsCoefficientTempRepository.save(any())).thenReturn(tempRow);

        //Act
        carInsCoefficientService.saveTempTable(inputLine);
    }

    @Test
    public void saveTempTable_Return_Row_From_Table_When_Successful() {
        //Arrange
        CarInsCoefficientTemp tempRow = new CarInsCoefficientTemp();

        ArrayList<String> inputLine = new ArrayList<>();
        inputLine.add("1");
        inputLine.add("1");
        inputLine.add("1");
        inputLine.add("1.1");


        Mockito.when(mockCarInsCoefficientTempRepository.save(any())).thenReturn(tempRow);

        //Act
        CarInsCoefficientTemp result = carInsCoefficientService.saveTempTable(inputLine);

        //Assert
        Assert.assertNotNull(result);
    }


    @Test
    public void setAllRows_To_Inactive_InCurrentDB() {
        //Arrange
        CarInsCoefficient row1 = new CarInsCoefficient();
        row1.setTableState(TableState.ACTIVE);
        CarInsCoefficient row2 = new CarInsCoefficient();
        row2.setTableState(TableState.ACTIVE);

        List<CarInsCoefficient> tempRow =new ArrayList<>();
        tempRow.add(row1);
        tempRow.add(row2);

        Mockito.when(mockCarInsCoefficientRepo.findAll()).thenReturn(tempRow);

        //Act
        carInsCoefficientService.setAllRowsAsInactive();
        List<CarInsCoefficient> result = carInsCoefficientService.findAllByTableStateActive();

        //Assert
        Assert.assertEquals(0,result.size());
    }
    @Test
    public void saveTempTable_To_Original() {

        //Arrange
        CarInsCoefficient row1 = new CarInsCoefficient();
        CarInsCoefficient row2 = new CarInsCoefficient();

        List<CarInsCoefficient> tempRow =new ArrayList<>();
        tempRow.add(row1);
        tempRow.add(row2);

        Mockito.when(mockCarInsCoefficientRepo.findAll()).thenReturn(tempRow);

        //Act
        carInsCoefficientService.saveTempTableToOriginal();
        List<CarInsCoefficient> result = carInsCoefficientService.findAllByTableStateActive();

        //Assert
        Assert.assertEquals(0,result.size());
    }
}
