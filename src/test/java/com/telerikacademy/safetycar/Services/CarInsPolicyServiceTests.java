
package com.telerikacademy.safetycar.Services;

import com.telerikacademy.safetycar.exceptions.CustomException;
import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.DTOs.CarInsPolicyDTO;
import com.telerikacademy.safetycar.models.Enums.PolicyState;
import com.telerikacademy.safetycar.models.Enums.Role;
import com.telerikacademy.safetycar.repositories.CarBaseAmountRepository;
import com.telerikacademy.safetycar.repositories.CarInsPolicyRepository;
import com.telerikacademy.safetycar.repositories.UserRepository;
import com.telerikacademy.safetycar.services.CarInsPolicyServiceImpl;

import com.telerikacademy.safetycar.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.Silent.class)

public class CarInsPolicyServiceTests {

    @Mock
    CarInsPolicyRepository mockCarInsPolicyRepo;

    @Mock
    CarBaseAmountRepository mockCarBaseAmountRepo;

    @Mock
    UserRepository mockUserRepo;

    @InjectMocks
    CarInsPolicyServiceImpl carInsPolicyService;

    @InjectMocks
    UserServiceImpl userService;


    private CarInsPolicy tempPendingPolicy() {
        CarInsPolicy pendingPolicy = new CarInsPolicy(car(), client(), LocalDate.parse("2017-11-10"), "index.jpg", 123.45, true, LocalDate.now().plusDays(1),
                LocalDateTime.now().minusDays(1), PolicyState.PENDING, 25, contact(), policyUser(), LocalDate.parse("2016-11-10"));
        pendingPolicy.setId(10L);
        return pendingPolicy;
    }

    private CarInsPolicy tempApprovedPolicy() {

        return new CarInsPolicy(car(), client(), LocalDate.parse("2017-11-10"), "index.jpg", 123.45, true, LocalDate.now().plusDays(1),
                LocalDateTime.now().minusDays(1), PolicyState.APPROVED, 25, contact(), policyUser(), LocalDate.parse("2016-11-10"));
    }

    private CarInsPolicy tempDeclinedPolicy() {

        return new CarInsPolicy(car(), client(), LocalDate.parse("2017-11-10"), "index.jpg", 123.45, true, LocalDate.now().plusDays(1),
                LocalDateTime.now().minusDays(1), PolicyState.DECLINED, 25, contact(), policyUser(), LocalDate.parse("2016-11-10"));
    }

    private PolicyUser policyUser() {

        return new PolicyUser("firstName", "lastName");
    }

    private ContactInfo contact() {

        return new ContactInfo("Varna", "1000", "25 Hajdushko horo.Str", "0889713670", "test@email.com");
    }

    private Car car() {

        return new Car("Audi", "A4", 1600);
    }

    private User admin() {

        User user = new User("emailAdmin@email.com", "password", "firstName", "lastName");
        user.setRole(Role.ROLE_ADMIN);
        user.setId(1L);
        return user;
    }

    private User agent() {

        User user = new User("emailAgent@email.com", "password", "firstName", "lastName");
        user.setRole(Role.ROLE_AGENT);
        user.setId(2L);
        return user;
    }

    private User client() {

        User user = new User("emailClient@email.com", "password", "firstName", "lastName");
        user.setRole(Role.ROLE_CLIENT);
        user.setId(3L);
        return user;
    }


    private CarInsPolicyDTO carDto() {

        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());

        return new CarInsPolicyDTO(car(), "firstName", "lastName", LocalDate.parse("2019-02-02"),
                LocalDate.parse("1990-01-01"), true, 123.45,
                firstFile, LocalDate.now(), "Sofia", "1000", "Adress", "9785236522",
                "client@email.com", LocalDate.parse("2000-10-10"));
    }


    @Test
    public void findAll_byID_should_return_CarInsPolicy_with_given_ID() {

        Mockito.when(mockCarInsPolicyRepo.findById(10L))
                .thenReturn(Optional.of(tempPendingPolicy()));

        Optional<CarInsPolicy> result = carInsPolicyService.findById(10L);

        Assert.assertEquals(tempPendingPolicy().getId(), result.get().getId());
    }

    @Test
    public void findAll_byEmail_should_return_CarIntPolicy_with_given_email() {
        List<CarInsPolicy> list = new ArrayList<>();
        list.add(tempPendingPolicy());

        String email = "test@mail.com";

        Mockito.when(mockCarInsPolicyRepo.findAllByUserEmail(email))
                .thenReturn(list);

        List<CarInsPolicy> result = carInsPolicyService.findAllByUserEmail(email);

        Assert.assertEquals(list, result);
    }

    @Test
    public void findAll_byPhoneNumber_should_return_CarInsPolicies_with_given_Number() {
        List<CarInsPolicy> list = new ArrayList<>();
        list.add(tempPendingPolicy());

        String phoneNumber = "test@mail.com";

        Mockito.when(mockCarInsPolicyRepo.findAllByPhoneNumber(phoneNumber))
                .thenReturn(list);

        List<CarInsPolicy> result = carInsPolicyService.findAllByPhoneNumber(phoneNumber);

        Assert.assertEquals(list, result);

    }

    @Test
    public void findAll_byPolicyUserEmail_should_return_CarIntPolicy_with_given_email() {
        List<CarInsPolicy> list = new ArrayList<>();
        list.add(tempPendingPolicy());

        String email = "emailAdmin@email.com";

        Mockito.when(mockCarInsPolicyRepo.findAllByPolicyUserEmail(email))
                .thenReturn(list);

        List<CarInsPolicy> result = carInsPolicyService.findAllByPolicyUserEmail(email);

        Assert.assertEquals(list, result);
    }

    @Test
    public void approveRequestById_Should_Change_PolicyStatus_To_Approved_When_Successful() throws Exception {
        //Arrange
        final long requestId = 10L;

        CarInsPolicy tempPendingPolicy = tempPendingPolicy();
        tempPendingPolicy.setId(requestId);

        Mockito.when(mockCarInsPolicyRepo.findById(10L)).thenReturn(Optional.of(tempPendingPolicy));
        tempPendingPolicy.setPolicyState(PolicyState.APPROVED);
        Mockito.when(mockCarInsPolicyRepo.save(tempPendingPolicy)).thenReturn(tempPendingPolicy);

        //Act
        Optional<CarInsPolicy> result = carInsPolicyService.approveRequestById(10L);

        //Arrange
        Assert.assertEquals(PolicyState.APPROVED, result.get().getPolicyState());
    }


    @Test
    public void declineRequestById_Should_Change_PolicyStatus_To_Declined_When_Successful() throws Exception {
        //Arrange
        final long requestId = 10L;

        CarInsPolicy tempPendingPolicy = tempPendingPolicy();
        tempPendingPolicy.setId(requestId);

        Mockito.when(mockCarInsPolicyRepo.findById(10L)).thenReturn(Optional.of(tempPendingPolicy));
        tempPendingPolicy.setPolicyState(PolicyState.DECLINED);
        Mockito.when(mockCarInsPolicyRepo.save(tempPendingPolicy)).thenReturn(tempPendingPolicy);

        //Act
        Optional<CarInsPolicy> result = carInsPolicyService.declineRequestById(10L);

        //Arrange
        Assert.assertEquals(PolicyState.DECLINED, result.get().getPolicyState());
    }

    @Test
    public void cancelRequestByUser_Should_Change_PolicyStatus_To_Inactive_andUserOwner_to_Admin() throws Exception {

        CarInsPolicy tempPendingPolicy = tempPendingPolicy();

        User admin = admin();

        Mockito.when(mockCarInsPolicyRepo.findById(10L)).thenReturn(Optional.of(tempPendingPolicy));
        Mockito.when(mockUserRepo.findById(1L)).thenReturn(Optional.of(admin));

        tempPendingPolicy.setPolicyState(PolicyState.UNACTIVE);
        tempPendingPolicy.setUser(admin());

        Mockito.when(mockCarInsPolicyRepo.save(tempPendingPolicy)).thenReturn(tempPendingPolicy);

        //Act
        Optional<CarInsPolicy> result = carInsPolicyService.cancelRequestById(10L);

        //Arrange
        Assert.assertEquals(Role.ROLE_ADMIN, result.get().getUser().getRole());
        Assert.assertEquals(PolicyState.UNACTIVE, result.get().getPolicyState());

    }


    @Test
    public void findById_Should_ReturnOptionalPolicy_When_MatchExists() {
        //Arrange
        CarInsPolicy policy = tempApprovedPolicy();

        Optional<CarInsPolicy> optional = Optional.of(policy);
        Mockito.when(mockCarInsPolicyRepo.findById(Mockito.anyLong())).thenReturn(optional);

        //Act
        Optional<CarInsPolicy> result = carInsPolicyService.findById(1);

        //Assert
        Assert.assertTrue(result.isPresent());
    }

    @Test
    public void findById_Should_ReturnEmptyOptional_When_PolicyDoesntExists() {
        //Arrange
        Optional<CarInsPolicy> optional = Optional.empty();
        Mockito.when(mockCarInsPolicyRepo.findById(Mockito.anyLong())).thenReturn(optional);

        //Act
        Optional<CarInsPolicy> result = carInsPolicyService.findById(1);

        //Assert
        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void save_Policy_Should_Return_Policy_When_Successful() {
        //Arrange
        CarInsPolicy policy = new CarInsPolicy();

        CarInsPolicyDTO dto = carDto();
        String image = "image";
        User client = client();
        ContactInfo info = contact();
        PolicyUser policyUser = policyUser();

        Mockito.when(mockCarBaseAmountRepo.findActiveRateDate()).thenReturn(LocalDate.parse("2000-10-10"));
        policy.setRateDate(LocalDate.parse("2000-10-10"));
        Mockito.when(mockCarInsPolicyRepo.save(any())).thenReturn(policy);

        //Act
        CarInsPolicy result = carInsPolicyService.save(dto, image, client, info, policyUser, 30);

        //Assert
        Assert.assertNotNull(result);
    }

    @Test(expected = ResponseStatusException.class)
    public void checkStartPolicyDate_Has_To_Throw_Exception_If_Is_After_Week() {

        //Arrange
        LocalDate startPolicyDate = LocalDate.now().plusDays(8);

        //Act;
       carInsPolicyService.checkStartPolicyDate(startPolicyDate);

    }


    @Test(expected = ResponseStatusException.class)
    public void checkStartPolicyDate_Has_To_Throw_Exception_If_Is_In_Past() {

        //Arrange
        LocalDate startPolicyDate = LocalDate.now().minusDays(1);

        //Act;
        carInsPolicyService.checkStartPolicyDate(startPolicyDate);

    }

}
